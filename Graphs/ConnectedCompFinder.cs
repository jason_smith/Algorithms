﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Graphs
{
    public class ConnectedCompFinder
    {
        readonly int[] _vertexCompIds;
        readonly int _numComponents;


        public ConnectedCompFinder(Graph graph)
        {
            _vertexCompIds = new int[graph.VertexCount];

            var visited = new bool[graph.VertexCount];
            for (int vertex = 0; vertex < graph.VertexCount; vertex++)
            {
                if (!visited[vertex])
                {
                    this.Dfs(graph, vertex, visited);
                    _numComponents++;
                }
            }
        }


        public int ComponentCount
        {
            get { return _numComponents; }
        }


        private void Dfs(Graph graph, int vertex, bool[] visited)
        {
            visited[vertex] = true;
            _vertexCompIds[vertex] = _numComponents;

            IEnumerable<int> adjVertices = graph.GetVerticesAdjacentTo(vertex);
            foreach (int adjVertex in adjVertices)
            {
                if (!visited[adjVertex])
                    this.Dfs(graph, adjVertex, visited);
            }
        }


        public int GetComponentId(int vertex)
        {
            return _vertexCompIds[vertex];
        }


        public bool IsConnected(int vertex1, int vertex2)
        {
            bool isConnected = (_vertexCompIds[vertex1] == _vertexCompIds[vertex2]);
            return isConnected;
        }
    }
}
