﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Graphs
{
    public interface IGraph
    {
        int EdgeCount { get; }
        int VertexCount { get; }

        //void AddEdge(int vertex1, int vertex2);
        IEnumerable<int> GetVerticesAdjacentTo(int sourceVertex);
    }
}
