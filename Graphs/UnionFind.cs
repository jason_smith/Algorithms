﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Graphs
{
    public class UnionFind
    {
        #region Fields

        int[] _parents;  // _parent[i] = parent of i
        int[] _sizes;    // _size[i] = number of sites in subtree rooted at i

        #endregion

        #region Constructors


        public UnionFind(int componentCount)
        {
            this.ComponentCount = componentCount;
            _parents = new int[componentCount];
            _sizes = new int[componentCount];

            for (int i = 0; i < componentCount; i++)
            {
                _parents[i] = i;
                _sizes[i] = 1;
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the number of components.
        /// </summary>
        public int ComponentCount { get; private set; }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the component identifier for the component containing the specified index.
        /// </summary>
        /// <param name="vertex">The index representing one object.</param>
        /// <returns></returns>
        public int Find(int vertex)
        {
            this.Validate(vertex);

            int parent = vertex;
            while (parent != _parents[parent])
                parent = _parents[parent];

            return parent;
        }


        public bool IsConnected(int vertex1, int vertex2)
        {
            return this.Find(vertex1) == this.Find(vertex2);
        }


        public void Union(int vertex1, int vertex2)
        {
            int root1 = this.Find(vertex1);
            int root2 = this.Find(vertex2);
            if (root1 == root2)
                return;

            // make smaller root point to larger one
            if (_sizes[root1] < _sizes[root2])
            {
                _parents[root1] = root2;
                _sizes[root2] += _sizes[root1];
            }
            else
            {
                _parents[root2] = root1;
                _sizes[root1] += _sizes[root2];
            }

            this.ComponentCount--;
        }


        private void Validate(int index)
        {
            int parentsLength = _parents.Length;
            if (index < 0 || index >= parentsLength)
                throw new IndexOutOfRangeException();
        }

        #endregion
    }
}
