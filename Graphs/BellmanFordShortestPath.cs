﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Graphs
{
    public class BellmanFordShortestPath
    {
        #region Fields

        readonly Queue<int> _queue;
        readonly DirectedEdge[] _edgeTo;
        readonly double[] _distTo;
        readonly bool[] _onQueue;
        IEnumerable<int> _cycle;
        int _cost;

        #endregion

        #region Constructors


        public BellmanFordShortestPath(EdgeWeightedDigraph graph, int sourceVertex)
        {
            _distTo = new double[graph.VertexCount];
            _edgeTo = new DirectedEdge[graph.VertexCount];
            _onQueue = new bool[graph.VertexCount];
            _queue = new Queue<int>();

            for (int vertex = 0; vertex < graph.VertexCount; vertex++)
                _distTo[vertex] = double.PositiveInfinity;

            _distTo[sourceVertex] = 0.0;
            _queue.Enqueue(sourceVertex);
            _onQueue[sourceVertex] = true;

            while (_queue.Count > 0)
            {
                int vertex = _queue.Dequeue();
                _onQueue[vertex] = false;
                this.Relax(graph, vertex);
            }
        }

        #endregion

        #region Properties


        public bool HasNegativeCycle
        {
            get { return _cycle != null; }
        }


        public IEnumerable<int> NegativeCycle
        {
            get { return _cycle; }
        }

        #endregion

        #region Methods


        private void FindNegativeCycle()
        {
            var shortestPathsTree = new EdgeWeightedDigraph(_edgeTo.Length);
            for (int vertex = 0; vertex < _edgeTo.Length; vertex++)
            {
                if (_edgeTo[vertex] != null)
                    shortestPathsTree.AddEdge(_edgeTo[vertex]);
            }

            var cycleFinder = new DirectedCycle(shortestPathsTree);
            _cycle = cycleFinder.Cycle;
        }


        public double GetDistanceTo(int vertex)
        {
            return _distTo[vertex];
        }


        public IEnumerable<DirectedEdge> GetPathTo(int vertex)
        {
            var path = new Stack<DirectedEdge>();

            if (this.HasPathTo(vertex))
            {
                for (DirectedEdge edge = _edgeTo[vertex]; edge != null; edge = _edgeTo[edge.SourceVertex])
                    path.Push(edge);
            }

            return path;
        }


        public bool HasPathTo(int vertex)
        {
            return _distTo[vertex] < double.PositiveInfinity;
        }


        private void Relax(EdgeWeightedDigraph graph, int vertex)
        {
            IEnumerable<DirectedEdge> adjEdges = graph.GetEdgesAdjacentTo(vertex);
            foreach (DirectedEdge edge in adjEdges)
            {
                int destVertex = edge.DestinationVertex;
                if (_distTo[destVertex] > _distTo[vertex] + edge.Weight)
                {
                    _distTo[destVertex] = _distTo[vertex] + edge.Weight;
                    _edgeTo[destVertex] = edge;

                    if (!_onQueue[destVertex])
                    {
                        _queue.Enqueue(destVertex);
                        _onQueue[destVertex] = true;
                    }
                }

                _cost++;
                if (_cost % graph.VertexCount == 0)
                    this.FindNegativeCycle();
            }
        }

        #endregion
    }
}
