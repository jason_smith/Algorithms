﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Graphs
{
    public class EdgeWeightedDigraph : IDigraph
    {
        #region Fields

        readonly LinkedList<DirectedEdge>[] _adjList;

        #endregion

        #region Constructors


        public EdgeWeightedDigraph(int numVertices)
        {
            _adjList = new LinkedList<DirectedEdge>[numVertices];
        }

        #endregion

        #region Properties


        public int EdgeCount { get; private set; }


        public int VertexCount
        {
            get { return _adjList.Length; }
        }

        #endregion

        #region Methods


        public void AddEdge(DirectedEdge edge)
        {
            LinkedList<DirectedEdge> edgeList = this.GetEdgeList(edge.SourceVertex);
            edgeList.AddLast(edge);
            this.EdgeCount++;
        }


        private LinkedList<DirectedEdge> GetEdgeList(int vertex)
        {
            LinkedList<DirectedEdge> list = _adjList[vertex];
            if (list == null)
            {
                list = new LinkedList<DirectedEdge>();
                _adjList[vertex] = list;
            }

            return list;
        }


        public IEnumerable<DirectedEdge> GetEdges()
        {
            var edges = new LinkedList<DirectedEdge>();
            for (int vertex = 0; vertex < this.VertexCount; vertex++)
            {
                LinkedList<DirectedEdge> adjEdges = this.GetEdgeList(vertex);
                foreach (DirectedEdge edge in adjEdges)
                    edges.AddLast(edge);
            }

            return edges;
        }


        public IEnumerable<DirectedEdge> GetEdgesAdjacentTo(int sourceVertex)
        {
            return this.GetEdgeList(sourceVertex);
        }


        public IEnumerable<int> GetVerticesAdjacentTo(int sourceVertex)
        {
            var adjVertices = new LinkedList<int>();

            IEnumerable<DirectedEdge> adjEdges = this.GetEdgeList(sourceVertex);
            foreach (DirectedEdge edge in adjEdges)
                adjVertices.AddLast(edge.DestinationVertex);

            return adjVertices;
        }

        #endregion
    }
}
