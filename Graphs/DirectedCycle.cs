﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Graphs
{
    public class DirectedCycle
    {
        #region Fields

        readonly bool[] _visited;
        readonly bool[] _onStack; // vertices on recursive callstack
        readonly int[] _edgeTo;
        Stack<int> _cycle; // vertices on a cycle, if one exists

        #endregion

        #region Constructors


        public DirectedCycle(IDigraph graph)
        {
            _visited = new bool[graph.VertexCount];
            _onStack = new bool[graph.VertexCount];
            _edgeTo = new int[graph.VertexCount];

            for (int vertex = 0; vertex < graph.VertexCount; vertex++)
            {
                if (!_visited[vertex])
                    this.Dfs(graph, vertex);
            }
        }

        #endregion

        #region Properties


        public IEnumerable<int> Cycle
        {
            get { return _cycle; }
        }


        public bool HasCycle
        {
            get { return _cycle != null; }
        }

        #endregion

        #region Methods


        private void Dfs(IDigraph graph, int vertex)
        {
            _visited[vertex] = true;
            _onStack[vertex] = true;

            IEnumerable<int> adjVertices = graph.GetVerticesAdjacentTo(vertex);
            foreach (int adjVertex in adjVertices)
            {
                if (this.HasCycle)
                    return;

                if (!_visited[adjVertex])
                {
                    _edgeTo[adjVertex] = vertex;
                    this.Dfs(graph, adjVertex);
                }
                else if (_onStack[adjVertex])
                {
                    _cycle = new Stack<int>();
                    for (int x = 0; x != adjVertex; x = _edgeTo[x])
                        _cycle.Push(x);

                    _cycle.Push(adjVertex);
                    _cycle.Push(vertex);
                }
            }

            _onStack[vertex] = false;
        }

        #endregion
    }
}
