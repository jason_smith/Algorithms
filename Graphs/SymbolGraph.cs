﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Graphs
{
    public class SymbolGraph
    {
        #region Fields

        readonly Dictionary<string, int> _symbolToVertexMap; // string -> index
        readonly string[] _invertedIndex; // index -> string
        readonly IGraph _graph;

        #endregion

        #region Constructors


        public SymbolGraph(IGraph graph, Dictionary<string, int> symbolToVertexMap, string[] invertedIndex)
        {
            _graph = graph;
            _symbolToVertexMap = symbolToVertexMap;
            _invertedIndex = invertedIndex;
        }

        #endregion

        #region Properties


        public IGraph Graph
        {
            get { return _graph; }
        }

        #endregion

        #region Methods


        public bool Contains(string name)
        {
            return _symbolToVertexMap.ContainsKey(name);
        }


        public string GetName(int vertex)
        {
            return _invertedIndex[vertex];
        }


        public int GetVertex(string name)
        {
            return _symbolToVertexMap[name];
        }


        public override string ToString()
        {
            var sb = new StringBuilder(_graph.VertexCount.ToString() + " vertices, " + _graph.EdgeCount.ToString() + " edges");
            sb.AppendLine();

            for (int srcVertex = 0; srcVertex < _graph.VertexCount; srcVertex++)
            {
                string symbol = _invertedIndex[srcVertex];
                sb.Append(symbol + ": ");

                IEnumerable<int> adjVertices = _graph.GetVerticesAdjacentTo(srcVertex);
                foreach (int adjVertex in adjVertices)
                {
                    symbol = _invertedIndex[adjVertex];
                    sb.Append(symbol + " ");
                }

                sb.AppendLine();
            }

            return sb.ToString();
        }

        #endregion
    }
}
