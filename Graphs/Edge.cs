﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Graphs
{
    public class Edge : IComparable<Edge>, IComparable
    {
        #region Fields

        readonly int _vertex1;
        readonly int _vertex2;
        readonly double _weight;

        #endregion

        #region Constructors


        public Edge(int vertex1, int vertex2, double weight)
        {
            _vertex1 = vertex1;
            _vertex2 = vertex2;
            _weight = weight;
        }

        #endregion

        #region Properties


        public int Vertex1
        {
            get { return _vertex1; }
        }


        public int Vertex2
        {
            get { return _vertex2; }
        }


        public double Weight
        {
            get { return _weight; }
        }

        #endregion

        #region Methods


        public int CompareTo(Edge obj)
        {
            if (_weight < obj.Weight)
                return -1;

            if (_weight > obj.Weight)
                return 1;

            return 0;
        }


        int IComparable.CompareTo(object obj)
        {
            var edge = obj as Edge;
            if (edge == null)
                throw new ArgumentException("'Edge' type expected.");

            return this.CompareTo(edge);
        }


        public int Other(int vertex)
        {
            if (vertex == _vertex1)
                return _vertex2;

            if (vertex == _vertex2)
                return _vertex1;

            throw new ArgumentException("Vertex not in edge.");
        }


        public override string ToString()
        {
            return string.Format("{0}-{1} {2}", _vertex1, _vertex2, _weight);
        }

        #endregion
    }
}
