﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Graphs
{
    public class DfsPathFinder
    {
        readonly bool[] _visited;
        readonly int[] _edgeTo;
        readonly int _srcVertex;

        public DfsPathFinder(Graph graph, int sourceVertex)
        {
            _srcVertex = sourceVertex;
            _visited = new bool[graph.VertexCount];
            _edgeTo = new int[graph.VertexCount];

            this.Dfs(graph, sourceVertex);
        }


        private void Dfs(Graph graph, int vertex)
        {
            _visited[vertex] = true;

            IEnumerable<int> adjVertices = graph.GetVerticesAdjacentTo(vertex);
            foreach (int adjVertex in adjVertices)
            {
                if (!_visited[adjVertex])
                {
                    _edgeTo[adjVertex] = vertex;
                    this.Dfs(graph, adjVertex);
                }
            }
        }


        public IEnumerable<int> GetPathTo(int destVertex)
        {
            if (!this.HasPathTo(destVertex))
                return new int[0];

            var path = new Stack<int>();
            for (int vertex = destVertex; vertex != _srcVertex; vertex = _edgeTo[vertex])
                path.Push(vertex);

            path.Push(_srcVertex);
            return path;
        }


        public bool HasPathTo(int destVertex)
        {
            return _visited[destVertex];
        }
    }
}
