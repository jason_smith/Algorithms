﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Graphs
{
    public class Topological
    {
        #region Fields

        readonly IEnumerable<int> _order;

        #endregion

        #region Constructors


        public Topological(IDigraph graph)
        {
            var cycleFinder = new DirectedCycle(graph);
            if (!cycleFinder.HasCycle)
            {
                var dfOrder = new DepthFirstOrder(graph);
                _order = dfOrder.ReversePostorder;
            }
        }

        #endregion

        #region Properties


        public bool IsDAG
        {
            get { return _order != null; }
        }

        /// <summary>
        /// Gets vertices in topological order.
        /// </summary>
        public IEnumerable<int> Order
        {
            get { return _order; }
        }

        #endregion
    }
}
