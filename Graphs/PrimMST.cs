﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Algorithms.Sorts;

namespace Algorithms.Graphs
{
    public class PrimMST
    {
        #region Fields

        public const double FloatingPointEpsilon = 1E-12;
        readonly IndexMinPriorityQueue<double> _crossingEdges;
        readonly Edge[] _edgeTo;
        readonly double[] _distTo;
        readonly bool[] _visited;

        #endregion

        #region Constructors


        public PrimMST(EdgeWeightedGraph graph)
        {
            _crossingEdges = new IndexMinPriorityQueue<double>(graph.VertexCount);
            _edgeTo = new Edge[graph.VertexCount];
            _distTo = new double[graph.VertexCount];
            _visited = new bool[graph.VertexCount];

            for (int vertex = 0; vertex < graph.VertexCount; vertex++)
                _distTo[vertex] = double.PositiveInfinity;

            for (int vertex = 0; vertex < graph.VertexCount; vertex++)
            {
                if (!_visited[vertex])
                    this.Prim(graph, vertex);
            }

            bool conditionsMet = this.CheckOptimalityConditions(graph);
            System.Diagnostics.Debug.Assert(conditionsMet, "Optimality conditions not met.");
        }

        #endregion

        #region Properties


        public IEnumerable<Edge> Edges
        {
            get
            {
                var mst = new LinkedList<Edge>();
                for (int vertex = 0; vertex < _edgeTo.Length; vertex++)
                {
                    Edge edge = _edgeTo[vertex];
                    if (edge != null)
                        mst.AddLast(edge);
                }

                return mst;
            }
        }

        #endregion

        #region Methods


        private bool CheckOptimalityConditions(EdgeWeightedGraph graph)
        {
            // check weight
            double totalWeight = 0.0;
            foreach (Edge edge in this.Edges)
                totalWeight += edge.Weight;

            if (Math.Abs(totalWeight - this.GetWeight()) > FloatingPointEpsilon)
                return false;

            // check that it is acyclic
            var uf = new UnionFind(graph.VertexCount);
            foreach (Edge edge in this.Edges)
            {
                if (uf.IsConnected(edge.Vertex1, edge.Vertex2))
                    return false;

                uf.Union(edge.Vertex1, edge.Vertex2);
            }

            // check that it is a spanning forest
            foreach (Edge edge in this.Edges)
            {
                if (!uf.IsConnected(edge.Vertex1, edge.Vertex2))
                    return false;
            }

            // check that it is a minimal spanning forest (cut optimality conditions)
            foreach (Edge edge in this.Edges)
            {
                // all edges in MST except 'edge'
                uf = new UnionFind(graph.VertexCount);
                foreach (Edge e in this.Edges)
                {
                    if (e != edge)
                        uf.Union(e.Vertex1, e.Vertex2);
                }

                // check that 'edge' is min weight edge in crossing cut
                foreach (Edge e in this.Edges)
                {
                    if (!uf.IsConnected(e.Vertex1, e.Vertex2))
                    {
                        if (e.Weight < edge.Weight)
                            return false;
                    }
                }
            }

            return true;
        }


        public double GetWeight()
        {
            double weight = 0.0;

            foreach (Edge edge in this.Edges)
                weight += edge.Weight;

            return weight;
        }


        private void Prim(EdgeWeightedGraph graph, int vertex)
        {
            _distTo[vertex] = 0;
            _crossingEdges.Enqueue(vertex, _distTo[vertex]);
            while (_crossingEdges.Count > 0)
            {
                int adjVertex = _crossingEdges.Dequeue();
                this.Scan(graph, adjVertex);
            }
        }


        private void Scan(EdgeWeightedGraph graph, int vertex)
        {
            _visited[vertex] = true;

            IEnumerable<Edge> adjEdges = graph.GetEdgesAdjacentTo(vertex);
            foreach (Edge edge in adjEdges)
            {
                int vertex2 = edge.Other(vertex);

                if (_visited[vertex2])
                    continue;

                if (edge.Weight < _distTo[vertex2])
                {
                    _distTo[vertex2] = edge.Weight;
                    _edgeTo[vertex2] = edge;

                    if (_crossingEdges.Contains(vertex2))
                        _crossingEdges.DecreaseItem(vertex2, _distTo[vertex2]);
                    else
                        _crossingEdges.Enqueue(vertex2, _distTo[vertex2]);
                }
            }
        }

        #endregion
    }
}
