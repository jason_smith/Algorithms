﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Graphs
{
    public class DirectedDfsReachability
    {
        #region Fields

        readonly bool[] _visited;

        #endregion

        #region Constructors


        public DirectedDfsReachability(Digraph graph, int sourceVertex)
        {
            _visited = new bool[graph.VertexCount];
            this.Dfs(graph, sourceVertex);
        }


        public DirectedDfsReachability(Digraph graph, IEnumerable<int> sourceVertices)
        {
            _visited = new bool[graph.VertexCount];

            foreach (int vertex in sourceVertices)
            {
                if (!_visited[vertex])
                    this.Dfs(graph, vertex);
            }
        }

        #endregion

        #region Methods


        private void Dfs(Digraph graph, int vertex)
        {
            _visited[vertex] = true;
            IEnumerable<int> adjVertices = graph.GetVerticesAdjacentTo(vertex);
            foreach (int adjVertex in adjVertices)
            {
                if (!_visited[adjVertex])
                    this.Dfs(graph, adjVertex);
            }
        }


        public bool IsReachable(int vertex)
        {
            return _visited[vertex];
        }

        #endregion
    }
}
