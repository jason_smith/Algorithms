﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Graphs
{
    public enum GraphType
    {
        Undirected,
        Directed
    }

    public static class GraphFactory
    {
        //private static IGraph BuildGraph(
        //    string graphText, 
        //    char delimiter, 
        //    Dictionary<string, int> symbolToVertexMap, 
        //    string[] invertedIndex,
        //    GraphType graphType)
        //{
        //    IGraph graph = CreateGraph(graphType, symbolToVertexMap.Count);

        //    using (var reader = new StringReader(graphText))
        //    {
        //        string line = reader.ReadLine();
        //        while (line != null)
        //        {
        //            string[] lineParts = line.Split(delimiter);
        //            int vertex = symbolToVertexMap[lineParts[0]];
        //            for (int i = 1; i < lineParts.Length; i++)
        //            {
        //                int adjVertex = symbolToVertexMap[lineParts[i]];
        //                graph.AddEdge(vertex, adjVertex);
        //            }

        //            line = reader.ReadLine();
        //        }
        //    }

        //    return graph;
        //}


        //public static Task<IGraph> BuildGraphAsync(string filePath, GraphType graphType)
        //{
        //    return Task.Run(() =>
        //    {
        //        IGraph graph = null;

        //        string graphText = File.ReadAllText(filePath);
        //        using (var reader = new StringReader(graphText))
        //        {
        //            string line = reader.ReadLine();
        //            int numVertices = int.Parse(line);
        //            graph = CreateGraph(graphType, numVertices);

        //            line = reader.ReadLine();
        //            int numEdges = int.Parse(line);

        //            for (int i = 0; i < numEdges; i++)
        //            {
        //                line = reader.ReadLine();
        //                string[] lineParts = line.Split(new char[1] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

        //                int vertex1 = int.Parse(lineParts[0]);
        //                int vertex2 = int.Parse(lineParts[1]);
        //                graph.AddEdge(vertex1, vertex2);
        //            }
        //        }

        //        return graph;
        //    });
        //}


        //public static Task<SymbolGraph> BuildSymbolGraphAsync(string filePath, char delimiter, GraphType graphType)
        //{
        //    if (string.IsNullOrEmpty(filePath))
        //        throw new ArgumentNullException("filePath");

        //    if (!File.Exists(filePath))
        //        throw new FileNotFoundException("File does not exist: " + filePath);

        //    return Task.Run(() =>
        //    {
        //        string graphText = File.ReadAllText(filePath);
        //        Dictionary<string, int> symbolToVertexMap = GetSymbolToVertexMap(graphText, delimiter);
        //        string[] invertedIndex = GetInvertedIndex(symbolToVertexMap);
        //        IGraph graph = BuildGraph(graphText, delimiter, symbolToVertexMap, invertedIndex, graphType);

        //        var symbolGraph = new SymbolGraph(graph, symbolToVertexMap, invertedIndex);
        //        return symbolGraph;
        //    });
        //}


        //private static IGraph CreateGraph(GraphType graphType, int vertexCount)
        //{
        //    switch (graphType)
        //    {
        //        case GraphType.Undirected:
        //            return new Graph(vertexCount);
        //        case GraphType.Directed:
        //            return new Digraph(vertexCount);
        //        default:
        //            throw new InvalidOperationException("Could not create graph.");
        //    }
        //}


        //private static string[] GetInvertedIndex(Dictionary<string, int> symbolToVertexMap)
        //{
        //    var invertedIndex = new string[symbolToVertexMap.Count];
        //    foreach (string symbol in symbolToVertexMap.Keys)
        //    {
        //        int vertex = symbolToVertexMap[symbol];
        //        invertedIndex[vertex] = symbol;
        //    }

        //    return invertedIndex;
        //}


        //private static Dictionary<string, int> GetSymbolToVertexMap(string graphText, char delimiter)
        //{
        //    var symbolToVertexMap = new Dictionary<string, int>();

        //    using (var reader = new StringReader(graphText))
        //    {
        //        string line = reader.ReadLine();
        //        while (line != null)
        //        {
        //            string[] lineParts = line.Split(delimiter);
        //            for (int i = 0; i < lineParts.Length; i++)
        //            {
        //                string part = lineParts[i];
        //                if (!symbolToVertexMap.ContainsKey(part))
        //                    symbolToVertexMap.Add(part, symbolToVertexMap.Count);
        //            }

        //            line = reader.ReadLine();
        //        }
        //    }

        //    return symbolToVertexMap;
        //}
    }
}
