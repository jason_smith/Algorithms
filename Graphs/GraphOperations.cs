﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Graphs
{
    public static class GraphOperations
    {

        public static int AverageDegree(Graph graph)
        {
            double avgDegree = 2.0 * graph.EdgeCount / graph.VertexCount;
            return (int)avgDegree;
        }


        public static int Degree(Graph graph, int vertex)
        {
            IEnumerable<int> adjVertices = graph.GetVerticesAdjacentTo(vertex);
            return adjVertices.Count();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// Assumes no self loops or parallel edges.
        /// </remarks>
        /// <param name="graph"></param>
        /// <returns></returns>
        public static bool HasCycle(Graph graph)
        {
            var visited = new bool[graph.VertexCount];

            for (int vertex = 0; vertex < graph.VertexCount; vertex++)
            {
                if (!visited[vertex])
                {
                    bool cycleDetected = HasCycle_Dfs(graph, vertex, visited);
                    if (cycleDetected)
                        return true;
                }
            }

            return false;
        }


        private static bool HasCycle_Dfs(Graph graph, int vertex, bool[] visited)
        {
            visited[vertex] = true;

            IEnumerable<int> adjVertices = graph.GetVerticesAdjacentTo(vertex);
            foreach (int adjVertex in adjVertices)
            {
                if (!visited[adjVertex])
                {
                    bool cycleDetected = HasCycle_Dfs(graph, adjVertex, visited);
                    if (cycleDetected)
                        return true;
                }
                else
                    return true;
            }

            return false;
        }


        public static bool IsBipartite(Graph graph)
        {
            var visited = new bool[graph.VertexCount];
            var color = new bool[graph.VertexCount];

            for (int vertex = 0; vertex < graph.VertexCount; vertex++)
            {
                if (!visited[vertex])
                {
                    bool isBipartite = IsBipartite_Dfs(graph, vertex, visited, color);
                    if (!isBipartite)
                        return false;
                }
            }

            return true;
        }


        private static bool IsBipartite_Dfs(Graph graph, int vertex, bool[] visited, bool[] color)
        {
            visited[vertex] = true;

            IEnumerable<int> adjVertices = graph.GetVerticesAdjacentTo(vertex);
            foreach (int adjVertex in adjVertices)
            {
                if (!visited[adjVertex])
                {
                    color[adjVertex] = !color[vertex];
                    bool isBipartite = IsBipartite_Dfs(graph, adjVertex, visited, color);
                    if (!isBipartite)
                        return false;
                }
                else if (color[vertex] == color[adjVertex])
                    return false;
            }

            return true;
        }


        public static int MaxDegree(Graph graph)
        {
            int max = 0;
            for (int vertex = 0; vertex < graph.VertexCount; vertex++)
            {
                int degree = Degree(graph, vertex);
                if (degree > max)
                    max = degree;
            }

            return max;
        }


        public static int SelfLoopCount(Graph graph)
        {
            int count = 0;
            for (int srcVertex = 0; srcVertex < graph.VertexCount; srcVertex++)
            {
                IEnumerable<int> adjVertices = graph.GetVerticesAdjacentTo(srcVertex);
                foreach (int adjVertex in adjVertices)
                {
                    if (adjVertex == srcVertex)
                        count++;
                }
            }

            count /= 2;
            return count;
        }
    }
}
