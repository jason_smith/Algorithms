﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Graphs
{
    public class AcyclicShortestPath
    {
        #region Fields

        readonly DirectedEdge[] _edgeTo;
        readonly double[] _distTo;

        #endregion

        #region Constructors


        public AcyclicShortestPath(EdgeWeightedDigraph graph, int sourceVertex)
        {
            _edgeTo = new DirectedEdge[graph.VertexCount];
            _distTo = new double[graph.VertexCount];

            for (int vertex = 0; vertex < graph.VertexCount; vertex++)
                _distTo[vertex] = double.PositiveInfinity;

            _distTo[sourceVertex] = 0.0;

            var top = new Topological(graph);
            foreach (int vertex in top.Order)
                this.Relax(graph, vertex);
        }

        #endregion

        #region Methods


        public double GetDistanceTo(int vertex)
        {
            return _distTo[vertex];
        }


        public IEnumerable<DirectedEdge> GetPathTo(int vertex)
        {
            var path = new Stack<DirectedEdge>();

            if (this.HasPathTo(vertex))
            {
                for (DirectedEdge edge = _edgeTo[vertex]; edge != null; edge = _edgeTo[edge.SourceVertex])
                    path.Push(edge);
            }

            return path;
        }


        public bool HasPathTo(int vertex)
        {
            return _distTo[vertex] < double.PositiveInfinity;
        }


        private void Relax(EdgeWeightedDigraph graph, int vertex)
        {
            IEnumerable<DirectedEdge> adjEdges = graph.GetEdgesAdjacentTo(vertex);
            foreach (DirectedEdge edge in adjEdges)
            {
                int destVertex = edge.DestinationVertex;
                if (_distTo[destVertex] > _distTo[vertex] + edge.Weight)
                {
                    _distTo[destVertex] = _distTo[vertex] + edge.Weight;
                    _edgeTo[destVertex] = edge;
                }
            }
        }

        #endregion
    }
}
