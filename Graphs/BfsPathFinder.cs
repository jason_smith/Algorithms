﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Graphs
{
    public class BfsPathFinder
    {
        readonly bool[] _visited;
        readonly int[] _edgeTo;
        readonly int _srcVertex;


        public BfsPathFinder(IGraph graph, int sourceVertex)
        {
            _srcVertex = sourceVertex;
            _visited = new bool[graph.VertexCount];
            _edgeTo = new int[graph.VertexCount];

            this.Bfs(graph, _srcVertex);
        }


        private void Bfs(IGraph graph, int vertex)
        {
            _visited[vertex] = true;

            var queue = new Queue<int>();
            queue.Enqueue(vertex);
            while (queue.Count > 0)
            {
                int vert = queue.Dequeue();
                IEnumerable<int> adjVertices = graph.GetVerticesAdjacentTo(vert);
                foreach (int adjVertex in adjVertices)
                {
                    if (!_visited[adjVertex])
                    {
                        _edgeTo[adjVertex] = vert;
                        _visited[adjVertex] = true;
                        queue.Enqueue(adjVertex);
                    }
                }
            }
        }


        public IEnumerable<int> GetPathTo(int destVertex)
        {
            if (!this.HasPathTo(destVertex))
                return new int[0];

            var path = new Stack<int>();
            for (int vertex = destVertex; vertex != _srcVertex; vertex = _edgeTo[vertex])
                path.Push(vertex);

            path.Push(_srcVertex);
            return path;
        }


        public bool HasPathTo(int destVertex)
        {
            return _visited[destVertex];
        }
    }
}
