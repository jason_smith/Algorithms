﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Graphs
{
    public class KosarajuSharirSCC
    {
        #region Fields

        readonly bool[] _visited;
        readonly int[] _vertexCompIds;
        readonly int _sccCount;

        #endregion

        #region Constructors


        public KosarajuSharirSCC(Digraph graph)
        {
            _visited = new bool[graph.VertexCount];
            _vertexCompIds = new int[graph.VertexCount];
            
            var dfsOrder = new DepthFirstOrder(graph.Reverse());
            foreach (int vertex in dfsOrder.ReversePostorder)
            {
                if (!_visited[vertex])
                {
                    this.Dfs(graph, vertex);
                    _sccCount++;
                }
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the number of strongly connected components.
        /// </summary>
        public int ComponentCount
        {
            get { return _sccCount; }
        }

        #endregion

        #region Methods


        private void Dfs(Digraph graph, int vertex)
        {
            _visited[vertex] = true;
            _vertexCompIds[vertex] = _sccCount;

            IEnumerable<int> adjVertices = graph.GetVerticesAdjacentTo(vertex);
            foreach (int adjVertex in adjVertices)
            {
                if (!_visited[adjVertex])
                    this.Dfs(graph, vertex);
            }
        }


        public int GetComponentId(int vertex)
        {
            return _vertexCompIds[vertex];
        }


        public bool IsStronglyConnected(int vertex1, int vertex2)
        {
            return _vertexCompIds[vertex1] == _vertexCompIds[vertex2];
        }

        #endregion
    }
}
