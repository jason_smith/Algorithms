﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Graphs
{
    public class Digraph : IDigraph
    {
        #region Fields

        readonly LinkedList<int>[] _adjList;
        int _edgeCount;

        #endregion

        #region Constructors


        public Digraph(int numVertices)
        {
            _adjList = new LinkedList<int>[numVertices];
        }

        #endregion

        #region Properties


        public int EdgeCount
        {
            get { return _edgeCount; }
        }


        public int VertexCount
        {
            get { return _adjList.Length; }
        }

        #endregion

        #region Methods


        public void AddEdge(int vertex1, int vertex2)
        {
            LinkedList<int> list = this.GetVertexList(vertex1);
            list.AddLast(vertex2);
            _edgeCount++;
        }


        private LinkedList<int> GetVertexList(int vertex)
        {
            LinkedList<int> list = _adjList[vertex];
            if (list == null)
            {
                list = new LinkedList<int>();
                _adjList[vertex] = list;
            }

            return list;
        }


        public IEnumerable<int> GetVerticesAdjacentTo(int sourceVertex)
        {
            return this.GetVertexList(sourceVertex);
        }


        public Digraph Reverse()
        {
            var digraph = new Digraph(this.VertexCount);
            for (int vertex1 = 0; vertex1 < this.VertexCount; vertex1++)
            {
                LinkedList<int> adjVertices = this.GetVertexList(vertex1);
                foreach (int vertex2 in adjVertices)
                    digraph.AddEdge(vertex2, vertex1);
            }

            return digraph;
        }


        public override string ToString()
        {
            var sb = new StringBuilder(this.VertexCount.ToString() + " vertices, " + this.EdgeCount.ToString() + " edges");
            sb.AppendLine();

            for (int srcVertex = 0; srcVertex < this.VertexCount; srcVertex++)
            {
                sb.Append(srcVertex.ToString() + ": ");

                LinkedList<int> list = this.GetVertexList(srcVertex);
                foreach (int adjVertex in list)
                    sb.Append(adjVertex.ToString() + " ");

                sb.AppendLine();
            }

            return sb.ToString();
        }

        #endregion
    }
}
