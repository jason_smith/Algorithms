﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Graphs
{
    public class DirectedEdge
    {
        #region Fields

        readonly double _weight;
        readonly int _srcVertex;
        readonly int _destVertex;

        #endregion

        #region Constructors


        public DirectedEdge(int sourceVertex, int destVertex, double weight)
        {
            _srcVertex = sourceVertex;
            _destVertex = destVertex;
            _weight = weight;
        }

        #endregion

        #region Properties


        public int DestinationVertex
        {
            get { return _destVertex; }
        }


        public int SourceVertex
        {
            get { return _srcVertex; }
        }


        public double Weight
        {
            get { return _weight; }
        }

        #endregion

        #region Methods


        public override string ToString()
        {
            return string.Format("{0}->{1}  {2}", _srcVertex, _destVertex, _weight);
        }

        #endregion
    }
}
