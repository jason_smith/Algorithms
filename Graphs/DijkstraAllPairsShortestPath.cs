﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Graphs
{
    public class DijkstraAllPairsShortestPath
    {
        #region Fields

        readonly DijkstraShortestPath[] _all;

        #endregion

        #region Constructors


        public DijkstraAllPairsShortestPath(EdgeWeightedDigraph graph)
        {
            _all = new DijkstraShortestPath[graph.VertexCount];

            for (int vertex = 0; vertex < graph.VertexCount; vertex++)
                _all[vertex] = new DijkstraShortestPath(graph, vertex);
        }

        #endregion

        #region Methods


        public double GetDistance(int sourceVertex, int destVertex)
        {
            return _all[sourceVertex].GetDistanceTo(destVertex);
        }


        public IEnumerable<DirectedEdge> GetPath(int sourceVertex, int destVertex)
        {
            return _all[sourceVertex].GetPathTo(destVertex);
        }

        #endregion
    }
}
