﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Graphs
{
    public class TransitiveClosure
    {
        readonly DirectedDfsReachability[] _reachabilityClients;

        public TransitiveClosure(Digraph graph)
        {
            _reachabilityClients = new DirectedDfsReachability[graph.VertexCount];
            for (int vertex = 0; vertex < graph.VertexCount; vertex++)
                _reachabilityClients[vertex] = new DirectedDfsReachability(graph, vertex);
        }


        public bool IsReachable(int vertex1, int vertex2)
        {
            bool isReachable = _reachabilityClients[vertex1].IsReachable(vertex2);
            return isReachable;
        }
    }
}
