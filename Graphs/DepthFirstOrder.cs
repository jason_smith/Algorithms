﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Graphs
{
    public class DepthFirstOrder
    {
        #region Fields

        readonly LinkedList<int> _preorder;
        readonly LinkedList<int> _postorder;
        readonly Stack<int> _reversePostorder;
        readonly bool[] _visited;

        #endregion

        #region Constructors


        public DepthFirstOrder(IDigraph graph)
        {
            _preorder = new LinkedList<int>();
            _postorder = new LinkedList<int>();
            _reversePostorder = new Stack<int>();
            _visited = new bool[graph.VertexCount];

            for (int vertex = 0; vertex < graph.VertexCount; vertex++)
            {
                if (!_visited[vertex])
                    this.Dfs(graph, vertex);
            }
        }

        #endregion

        #region Properties


        public IEnumerable<int> Postorder
        {
            get { return _postorder; }
        }


        public IEnumerable<int> Preorder
        {
            get { return _preorder; }
        }


        public IEnumerable<int> ReversePostorder
        {
            get { return _reversePostorder; }
        }

        #endregion

        #region Methods


        private void Dfs(IDigraph graph, int vertex)
        {
            _visited[vertex] = true;
            _preorder.AddLast(vertex);

            IEnumerable<int> adjVertices = graph.GetVerticesAdjacentTo(vertex);
            foreach (int adjVertex in adjVertices)
            {
                if (!_visited[adjVertex])
                    this.Dfs(graph, adjVertex);
            }

            _postorder.AddLast(vertex);
            _reversePostorder.Push(vertex);
        }

        #endregion
    }
}
