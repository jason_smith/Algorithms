﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Algorithms.Sorts;

namespace Algorithms.Graphs
{
    public class KruskalMST
    {
        #region Fields

        readonly Queue<Edge> _mst;
        readonly double _weight;

        #endregion

        #region Constructors


        public KruskalMST(EdgeWeightedGraph graph)
        {
            _mst = new Queue<Edge>();

            var minPQ = new MinPriorityQueue<Edge>();
            foreach (Edge edge in graph.GetEdges())
                minPQ.Enqueue(edge);

            var uf = new UnionFind(graph.VertexCount);
            while (minPQ.Count > 0 && _mst.Count < graph.VertexCount - 1)
            {
                Edge e = minPQ.Dequeue();                   // get min weight edge.
                if (uf.IsConnected(e.Vertex1, e.Vertex2))   // ignore ineligible edges -- those that create a cycle.
                    continue;

                uf.Union(e.Vertex1, e.Vertex2);             // merge components
                _mst.Enqueue(e);                            // add edge to mst
                _weight += e.Weight;
            }

            bool conditionsMet = this.CheckOptimalityConditions(graph);
            System.Diagnostics.Debug.Assert(conditionsMet);
        }

        #endregion

        #region Properties


        public IEnumerable<Edge> Edges
        {
            get { return _mst; }
        }


        public double Weight
        {
            get { return _weight; }
        }

        #endregion

        #region Methods


        private bool CheckOptimalityConditions(EdgeWeightedGraph graph)
        {
            // check weight
            double totalWeight = 0.0;
            foreach (Edge edge in this.Edges)
                totalWeight += edge.Weight;

            if (Math.Abs(totalWeight - this.Weight) > PrimMST.FloatingPointEpsilon)
                return false;

            // check that it is acyclic
            var uf = new UnionFind(graph.VertexCount);
            foreach (Edge edge in this.Edges)
            {
                if (uf.IsConnected(edge.Vertex1, edge.Vertex2))
                    return false;

                uf.Union(edge.Vertex1, edge.Vertex2);
            }

            // check that it is a spanning forest
            foreach (Edge edge in this.Edges)
            {
                if (!uf.IsConnected(edge.Vertex1, edge.Vertex2))
                    return false;
            }

            // check that it is a minimal spanning forest (cut optimality conditions)
            foreach (Edge edge in this.Edges)
            {
                // all edges in MST except 'edge'
                uf = new UnionFind(graph.VertexCount);
                foreach (Edge e in this.Edges)
                {
                    if (e != edge)
                        uf.Union(e.Vertex1, e.Vertex2);
                }

                // check that 'edge' is min weight edge in crossing cut
                foreach (Edge e in this.Edges)
                {
                    if (!uf.IsConnected(e.Vertex1, e.Vertex2))
                    {
                        if (e.Weight < edge.Weight)
                            return false;
                    }
                }
            }

            return true;
        }

        #endregion
    }
}
