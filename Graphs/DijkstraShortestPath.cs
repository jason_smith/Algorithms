﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Algorithms.Sorts;

namespace Algorithms.Graphs
{
    public class DijkstraShortestPath
    {
        #region Fields

        readonly IndexMinPriorityQueue<double> _queue;
        readonly DirectedEdge[] _edgeTo;
        readonly double[] _distTo;

        #endregion

        #region Constructors


        public DijkstraShortestPath(EdgeWeightedDigraph graph, int sourceVertex)
        {
            _queue = new IndexMinPriorityQueue<double>(graph.VertexCount);
            _edgeTo = new DirectedEdge[graph.VertexCount];
            _distTo = new double[graph.VertexCount];

            for (int vertex = 0; vertex < graph.VertexCount; vertex++)
                _distTo[vertex] = double.PositiveInfinity;

            _distTo[sourceVertex] = 0.0;

            _queue.Enqueue(sourceVertex, 0.0);
            while (_queue.Count > 0)
                this.Relax(graph, _queue.Dequeue());
        }

        #endregion

        #region Methods


        public double GetDistanceTo(int vertex)
        {
            return _distTo[vertex];
        }


        public IEnumerable<DirectedEdge> GetPathTo(int vertex)
        {
            var path = new Stack<DirectedEdge>();

            if (this.HasPathTo(vertex))
            {
                for (DirectedEdge edge = _edgeTo[vertex]; edge != null; edge = _edgeTo[edge.SourceVertex])
                    path.Push(edge);
            }

            return path;
        }


        public bool HasPathTo(int vertex)
        {
            return _distTo[vertex] < double.PositiveInfinity;
        }


        private void Relax(EdgeWeightedDigraph graph, int vertex)
        {
            IEnumerable<DirectedEdge> adjEdges = graph.GetEdgesAdjacentTo(vertex);
            foreach (DirectedEdge edge in adjEdges)
            {
                int destVertex = edge.DestinationVertex;
                if (_distTo[destVertex] > _distTo[vertex] + edge.Weight)
                {
                    _distTo[destVertex] = _distTo[vertex] + edge.Weight;
                    _edgeTo[destVertex] = edge;

                    if (_queue.Contains(destVertex))
                        _queue.DecreaseItem(destVertex, _distTo[destVertex]);
                    else
                        _queue.Enqueue(destVertex, _distTo[destVertex]);
                }
            }
        }

        #endregion
    }
}
