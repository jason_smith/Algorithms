﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Graphs
{
    public class EdgeWeightedGraph : IGraph
    {
        #region Fields

        readonly int _numVertices;
        LinkedList<Edge>[] _adjList;
        int _numEdges;

        #endregion

        #region Constructors


        public EdgeWeightedGraph(int numVertices)
        {
            _numVertices = numVertices;
            _adjList = new LinkedList<Edge>[numVertices];
        }

        #endregion

        #region Properties


        public int EdgeCount
        {
            get { throw new NotImplementedException(); }
        }


        public int VertexCount
        {
            get { return _numVertices; }
        }

        #endregion

        #region Methods


        public void AddEdge(Edge edge)
        {
            LinkedList<Edge> edgeList = this.GetEdgeList(edge.Vertex1);
            edgeList.AddLast(edge);

            edgeList = this.GetEdgeList(edge.Vertex2);
            edgeList.AddLast(edge);

            _numEdges++;
        }


        private LinkedList<Edge> GetEdgeList(int vertex)
        {
            LinkedList<Edge> list = _adjList[vertex];
            if (list == null)
            {
                list = new LinkedList<Edge>();
                _adjList[vertex] = list;
            }

            return list;
        }


        public IEnumerable<Edge> GetEdges()
        {
            var allEdges = new LinkedList<Edge>();
            for (int vertex = 0; vertex < _numVertices; vertex++)
            {
                int selfLoops = 0;

                LinkedList<Edge> adjEdges = this.GetEdgeList(vertex);
                foreach (Edge edge in adjEdges)
                {
                    int otherVertex = edge.Other(vertex);
                    if (otherVertex > vertex)
                        allEdges.AddLast(edge);
                    else if (otherVertex == vertex)
                    {
                        if (selfLoops % 2 == 0)
                            allEdges.AddLast(edge);

                        selfLoops++;
                    }
                }
            }

            return allEdges;
        }


        public IEnumerable<Edge> GetEdgesAdjacentTo(int sourceVertex)
        {
            return this.GetEdgeList(sourceVertex);
        }


        public IEnumerable<int> GetVerticesAdjacentTo(int sourceVertex)
        {
            var adjVertices = new LinkedList<int>();

            IEnumerable<Edge> adjEdges = this.GetEdgeList(sourceVertex);
            foreach (Edge edge in adjEdges)
                adjVertices.AddLast(edge.Other(sourceVertex));

            return adjVertices;
        }


        public override string ToString()
        {
            var sb = new StringBuilder(this.VertexCount.ToString() + " vertices, " + this.EdgeCount.ToString() + " edges");
            sb.AppendLine();

            for (int vertex = 0; vertex < this.VertexCount; vertex++)
            {
                sb.Append(vertex + ": ");

                LinkedList<Edge> adjEdges = this.GetEdgeList(vertex);
                foreach (Edge edge in adjEdges)
                    sb.Append(edge + "  ");

                sb.AppendLine();
            }

            return sb.ToString();
        }

        #endregion
    }
}
