﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Sorts
{
    public static class Quick
    {
        private static int Partition(IComparable[] collection, int lo, int hi)
        {
            int left = lo;
            int right = hi + 1;

            IComparable partitioningItem = collection[left];
            while (true)
            {
                while (SortOperations.LessThan(collection[++left], partitioningItem))
                {
                    if (left == hi)
                        break;
                }

                while (SortOperations.LessThan(partitioningItem, collection[--right]))
                {
                    if (right == lo)
                        break;
                }

                if (left >= right)
                    break;

                SortOperations.Exchange(collection, left, right);
            }

            SortOperations.Exchange(collection, lo, right);
            return right;
        }


        public static void Sort(IComparable[] collection)
        {
            StdRandom.Shuffle(collection);
            Sort(collection, 0, collection.Length - 1);
        }


        private static void Sort(IComparable[] collection, int lo, int hi)
        {
            if (hi <= lo)
                return;

            int j = Partition(collection, lo, hi);
            Sort(collection, lo, j - 1);
            Sort(collection, j + 1, hi);
        }
    }
}
