﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Sorts
{
    public static class SortOperations
    {
        public static void Exchange(IComparable[] items, int index1, int index2)
        {
            IComparable temp = items[index1];
            items[index1] = items[index2];
            items[index2] = temp;
        }

        public static bool IsEqual(IComparable x, IComparable y)
        {
            return x.CompareTo(y) == 0;
        }

        public static bool IsSorted(IComparable[] items)
        {
            for (int i = 1; i < items.Length; i++)
            {
                if (LessThan(items[i], items[i - 1]))
                    return false;
            }

            return true;
        }

        public static bool LessThan(IComparable x, IComparable y)
        {
            return x.CompareTo(y) < 0;
        }

        public static void ShowInConsole(IComparable[] items)
        {
            foreach (IComparable item in items)
                Console.Write(item + " ");

            Console.WriteLine();
        }
    }
}
