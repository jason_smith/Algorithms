﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Sorts
{
    public static class MergeRecursive
    {
        private static void Merge(IComparable[] collection, IComparable[] temp, int lo, int mid, int hi)
        {
            // copy to sub-array to temp
            for (int i = lo; i <= hi; i++)
                temp[i] = collection[i];

            int leftIndex = lo;
            int rightIndex = mid + 1;
            for (int i = lo; i <= hi; i++)
            {
                if (leftIndex > mid)
                    collection[i] = temp[rightIndex++];
                else if (rightIndex > hi)
                    collection[i] = temp[leftIndex++];
                else if (SortOperations.LessThan(temp[rightIndex], temp[leftIndex]))
                    collection[i] = temp[rightIndex++];
                else
                    collection[i] = temp[leftIndex++];
            }
        }

        public static void Sort(IComparable[] collection)
        {
            var temp = new IComparable[collection.Length];
            Sort(collection, temp, 0, collection.Length - 1);
        }

        private static void Sort(IComparable[] collection, IComparable[] temp, int lo, int hi)
        {
            if (hi <= lo)
                return;

            int mid = (hi - lo) / 2 + lo;
            Sort(collection, temp, lo, mid);
            Sort(collection, temp, mid + 1, hi);
            Merge(collection, temp, lo, mid, hi);
        }
    }
}
