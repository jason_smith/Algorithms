﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Sorts
{
    public static class MergeIterative
    {
        private static void Merge(IComparable[] collection, IComparable[] temp, int lo, int mid, int hi)
        {
            // copy to sub-array to temp
            for (int i = lo; i <= hi; i++)
                temp[i] = collection[i];

            int leftIndex = lo;
            int rightIndex = mid + 1;
            for (int i = lo; i <= hi; i++)
            {
                if (leftIndex > mid)
                    collection[i] = temp[rightIndex++];
                else if (rightIndex > hi)
                    collection[i] = temp[leftIndex++];
                else if (SortOperations.LessThan(temp[rightIndex], temp[leftIndex]))
                    collection[i] = temp[rightIndex++];
                else
                    collection[i] = temp[leftIndex++];
            }
        }

        public static void Sort(IComparable[] collection)
        {
            var temp = new IComparable[collection.Length];

            for (int size = 1; size < collection.Length; size += size)
            {
                for (int lo = 0; lo < collection.Length - size; lo += size + size)
                    Merge(collection, temp, lo, lo + size - 1, Math.Min(lo + size + size - 1, collection.Length - 1));
            }
        }
    }
}
