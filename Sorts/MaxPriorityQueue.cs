﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Sorts
{
    public class MaxPriorityQueue<T> : PriorityQueue<T> where T : IComparable<T>
    {
        #region Constructors


        public MaxPriorityQueue(IComparer<T> comparer = null)
            : base(comparer)
        {
        }


        public MaxPriorityQueue(int initCapacity, IComparer<T> comparer = null)
            : base(initCapacity, comparer)
        {
        }


        public MaxPriorityQueue(IList<T> items, IComparer<T> comparer = null)
            : base(items, comparer)
        {
        }

        #endregion

        #region Methods


        protected override void Sink(int index)
        {
            while (2 * index <= this.Count)
            {
                int j = 2 * index;
                if (j < this.Count && this.IsLess(j, j + 1))
                    j++;

                if (!this.IsLess(index, j))
                    break;

                this.Exchange(index, j);
                index = j;
            }
        }


        protected override void Swim(int index)
        {
            while (index > 1 && this.IsLess(index / 2, index))
            {
                this.Exchange(index, index / 2);
                index = index / 2;
            }
        }

        #endregion
    }
}
