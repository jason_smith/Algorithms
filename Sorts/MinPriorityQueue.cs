﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Sorts
{
    public class MinPriorityQueue<T> : PriorityQueue<T> where T : IComparable<T>
    {
        #region Constructors


        public MinPriorityQueue(IComparer<T> comparer = null) : base(comparer)
        {
        }


        public MinPriorityQueue(int initCapacity, IComparer<T> comparer = null) : base(initCapacity, comparer)
        {
        }


        public MinPriorityQueue(IList<T> items, IComparer<T> comparer = null) : base(items, comparer)
        {
        }

        #endregion

        #region Methods


        protected override void Sink(int index)
        {
            while (2 * index <= this.Count)
            {
                int j = 2 * index;
                if (j < this.Count && this.IsLess(j + 1, j))
                    j++;

                if (!this.IsLess(j, index))
                    break;

                this.Exchange(j, index);
                index = j;
            }
        }


        protected override void Swim(int index)
        {
            while (index > 1 && this.IsLess(index, index / 2))
            {
                this.Exchange(index / 2, index);
                index = index / 2;
            }
        }

        #endregion
    }
}
