﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Sorts
{
    public abstract class PriorityQueue<T> where T : IComparable<T>
    {
        #region Fields

        readonly IComparer<T> _comparer;
        T[] _queue;

        #endregion

        #region Constructors


        protected PriorityQueue(IComparer<T> comparer = null)
            : this(1, comparer)
        {
        }


        protected PriorityQueue(int initCapacity, IComparer<T> comparer = null)
        {
            _comparer = comparer;
            _queue = new T[initCapacity + 1];
        }


        protected PriorityQueue(IList<T> items, IComparer<T> comparer = null)
        {
            _comparer = comparer;

            this.Count = items.Count;
            _queue = new T[items.Count + 1];

            for (int i = 0; i < items.Count; i++)
                _queue[i + 1] = items[i];

            for (int i = this.Count / 2; i >= 1; i--)
                this.Sink(i);
        }

        #endregion

        #region Properties


        public int Count { get; private set; }

        #endregion

        #region Methods


        public T Dequeue()
        {
            if (this.Count == 0)
                throw new InvalidOperationException("Priority queue is empty.");

            T max = _queue[1];
            this.Exchange(1, this.Count--);
            this.Sink(1);
            _queue[this.Count + 1] = default(T);

            if ((this.Count > 0) && (this.Count == (_queue.Length - 1) / 4))
                this.Resize(_queue.Length / 2);

            return max;
        }


        public void Enqueue(T item)
        {
            if (this.Count >= _queue.Length - 1)
                this.Resize(2 * _queue.Length);

            _queue[++this.Count] = item;
            this.Swim(this.Count);
        }


        protected void Exchange(int index1, int index2)
        {
            T temp = _queue[index1];
            _queue[index1] = _queue[index2];
            _queue[index2] = temp;
        }


        protected bool IsLess(int index1, int index2)
        {
            bool isLess;

            if (_comparer == null)
                isLess = _queue[index1].CompareTo(_queue[index2]) < 0;
            else
                isLess = _comparer.Compare(_queue[index1], _queue[index2]) < 0;

            return isLess;
        }


        public T Peek()
        {
            if (this.Count == 0)
                throw new InvalidOperationException("Priority queue is empty.");

            return _queue[1];
        }


        private void Resize(int capacity)
        {
            T[] temp = new T[capacity];
            if (capacity > _queue.Length)
                Array.Copy(_queue, temp, _queue.Length);
            else
                Array.Copy(_queue, 0, temp, 0, capacity);

            _queue = temp;
        }


        protected abstract void Sink(int index);


        protected abstract void Swim(int index);

        #endregion
    }
}
