﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Sorts
{
    public class IndexMinPriorityQueue<T> : IEnumerable<int> where T : IComparable
    {
        #region Fields

        readonly T[] _items;            // _items[i] = priority of i
        readonly int[] _priorityQueue;  // binary heap using 1-based indexing
        readonly int[] _reversePQ;      // inverse of _priorityQueue: _reversePQ[_priorityQueue[i]] = _priorityQueue[_reversePQ[i]] = i
        readonly int _capacity;         // max number of elements on priority queue

        #endregion

        #region Constructors


        public IndexMinPriorityQueue(int capacity)
        {
            if (_capacity < 0)
                throw new ArgumentException("Capacity must be greater than zero.");

            _capacity = capacity;
            _items = new T[_capacity + 1];
            _priorityQueue = new int[_capacity + 1];
            _reversePQ = new int[_capacity + 1];

            for (int i = 0; i <= _capacity; i++)
                _reversePQ[i] = -1;
        }

        #endregion

        #region Properties


        public int Count { get; private set; }

        #endregion

        #region Methods


        public void ChangeItem(int index, T item)
        {
            this.ValidateIndexRange(index);

            if (!this.Contains(index))
                throw new KeyNotFoundException();

            _items[index] = item;
            this.Swim(_reversePQ[index]);
            this.Sink(_reversePQ[index]);
        }


        public bool Contains(int i)
        {
            if (i < 0 || i >= this.Count)
                return false;

            return _reversePQ[i] != -1;
        }


        public void DecreaseItem(int index, T item)
        {
            this.ValidateIndexRange(index);

            if (!this.Contains(index))
                throw new KeyNotFoundException();

            if (_items[index].CompareTo(item) <= 0)
                throw new ArgumentException("Calling DecreaseItem() with given argument would not strictly decrease the item.");

            _items[index] = item;
            this.Swim(_reversePQ[index]);
        }


        public int Dequeue()
        {
            this.ValidateNotEmpty();

            int min = _priorityQueue[1];
            this.Exchange(1, this.Count--);
            this.Sink(1);

            _reversePQ[min] = -1;
            _items[min] = default(T);
            _priorityQueue[this.Count + 1] = -1;
            return min;
        }


        public void Enqueue(int index, T item)
        {
            this.ValidateIndexRange(index);
            
            if (this.Contains(index))
                throw new ArgumentException("Index is already in priority queue.");

            this.Count++;
            _reversePQ[index] = this.Count;
            _priorityQueue[this.Count] = index;
            _items[index] = item;
            this.Swim(this.Count);
        }


        private void Exchange(int i, int j)
        {
            int temp = _priorityQueue[i];
            _priorityQueue[i] = _priorityQueue[j];
            _priorityQueue[j] = temp;

            _reversePQ[_priorityQueue[i]] = i;
            _reversePQ[_priorityQueue[j]] = j;
        }


        public IEnumerator<int> GetEnumerator()
        {
            var copy = new IndexMinPriorityQueue<T>(_priorityQueue.Length - 1);
            for (int i = 1; i <= this.Count; i++)
                copy.Enqueue(_priorityQueue[i], _items[_priorityQueue[i]]);

            while (copy.Count > 0)
            {
                yield return copy.Dequeue();
            }
        }


        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }


        public T GetItemAt(int index)
        {
            this.ValidateIndexRange(index);

            if (!this.Contains(index))
                throw new KeyNotFoundException();

            return _items[index];
        }


        public int GetMinIndex()
        {
            this.ValidateNotEmpty();
            return _priorityQueue[1];
        }


        public T GetMinItem()
        {
            this.ValidateNotEmpty();
            return _items[_priorityQueue[1]];
        }


        private bool GreaterThan(int i, int j)
        {
            return _items[_priorityQueue[i]].CompareTo(_items[_priorityQueue[j]]) > 0;
        }


        public void IncreaseItem(int index, T item)
        {
            this.ValidateIndexRange(index);

            if (!this.Contains(index))
                throw new KeyNotFoundException();

            if (_items[index].CompareTo(item) >= 0)
                throw new ArgumentException("Calling IncreaseItem() with given argument would not strictly increase the item.");

            _items[index] = item;
            this.Sink(_reversePQ[index]);
        }


        public void Remove(int index)
        {
            this.ValidateIndexRange(index);

            if (!this.Contains(index))
                throw new KeyNotFoundException();

            int i = _reversePQ[index];
            this.Count--;
            this.Exchange(i, this.Count);
            this.Swim(i);
            this.Sink(i);
            _items[index] = default(T);
            _reversePQ[index] = -1;
        }


        private void Sink(int index)
        {
            while (2 * index <= this.Count)
            {
                int j = 2 * index;
                if (j < this.Count && this.GreaterThan(j, j + 1))
                    j++;

                if (!this.GreaterThan(index, j))
                    break;

                this.Exchange(index, j);
                index = j;
            }
        }


        private void Swim(int index)
        {
            while (index > 1 && this.GreaterThan(index / 2, index))
            {
                this.Exchange(index, index / 2);
                index /= 2;
            }
        }


        private void ValidateNotEmpty()
        {
            if (this.Count == 0)
                throw new InvalidOperationException("Priority queue is empty.");
        }


        private void ValidateIndexRange(int index)
        {
            if (index < 0 || index >= _capacity)
                throw new IndexOutOfRangeException();
        }

        #endregion
    }
}
