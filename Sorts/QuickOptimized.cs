﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Sorts
{
    public static class QuickOptimized
    {
        readonly static int InsertionCutoff = 8;
        readonly static int MedianThreeCutoff = 40;

        /// <summary>
        /// Gets the index of the median element among the three specified indices.
        /// </summary>
        private static int MedianThree(IComparable[] collection, int i, int j, int k)
        {
            if (SortOperations.LessThan(collection[i], collection[j]))
            {
                if (SortOperations.LessThan(collection[j], collection[k]))
                    return j;

                if (SortOperations.LessThan(collection[i], collection[k]))
                    return k;

                return i;
            }
            else
            {
                if (SortOperations.LessThan(collection[k], collection[j]))
                    return j;

                if (SortOperations.LessThan(collection[k], collection[i]))
                    return k;

                return i;
            }
        }

        public static void Sort(IComparable[] collection)
        {
            Sort(collection, 0, collection.Length - 1);
        }

        private static void Sort(IComparable[] collection, int lo, int hi)
        {
            int length = hi - lo + 1;

            if (length <= InsertionCutoff) // cutoff to insertion sort
            {
                Insertion.Sort(collection, lo, hi);
                return;
            }
            else if (length <= MedianThreeCutoff) // use median-of-3 as partitioning element
            {
                int median = MedianThree(collection, lo, lo + length / 2, hi);
                SortOperations.Exchange(collection, median, lo);
            }
            else // use Tukey ninther as partitioning element
            {
                int eps = length / 8;
                int mid = lo + length / 2;
                int median1 = MedianThree(collection, lo, lo + eps, lo + eps + eps);
                int median2 = MedianThree(collection, mid - eps, mid, mid + eps);
                int median3 = MedianThree(collection, hi - eps - eps, hi - eps, hi);
                int ninther = MedianThree(collection, median1, median2, median3);
                SortOperations.Exchange(collection, ninther, lo);
            }

            // Bently-McIlroy 3-way partitioning
            int i = lo;
            int j = hi + 1;
            int p = i;
            int q = j;

            IComparable partitioningItem = collection[lo];
            while (true)
            {
                while (SortOperations.LessThan(collection[++i], partitioningItem))
                {
                    if (i == hi)
                        break;
                }

                while (SortOperations.LessThan(partitioningItem, collection[--j]))
                {
                    if (j == lo)
                        break;
                }

                // pointers cross
                if (i == j && SortOperations.IsEqual(collection[i], partitioningItem))
                    SortOperations.Exchange(collection, ++p, i);

                if (i >= j)
                    break;

                SortOperations.Exchange(collection, i, j);
                if (SortOperations.IsEqual(collection[i], partitioningItem))
                    SortOperations.Exchange(collection, ++p, i);

                if (SortOperations.IsEqual(collection[j], partitioningItem))
                    SortOperations.Exchange(collection, --q, j);
            }

            i = j + 1;
            for (int k = lo; k <= p; k++)
                SortOperations.Exchange(collection, k, j--);

            for (int k = hi; k >= q; k--)
                SortOperations.Exchange(collection, k, i++);

            Sort(collection, lo, j);
            Sort(collection, i, hi);
        }
    }
}
