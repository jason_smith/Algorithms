﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Sorts
{
    public static class Shell
    {
        public static void Sort(IComparable[] collection)
        {
            int arrayLength = collection.Length;
            int sortInterval = 1;

            while (sortInterval < arrayLength / 3)
                sortInterval = 3 * sortInterval + 1;

            while (sortInterval >= 1)
            {
                // h-sort the array
                for (int i = sortInterval; i < arrayLength; i++)
                {
                    for (int j = i; j >= sortInterval && SortOperations.LessThan(collection[j], collection[j - sortInterval]); j -= sortInterval)
                        SortOperations.Exchange(collection, j, j - sortInterval);
                }

                sortInterval /= 3;
            }
        }
    }
}
