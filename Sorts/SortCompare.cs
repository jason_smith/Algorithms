﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Sorts
{
    public static class SortCompare
    {
        public static double TimeRandomInput(string alg, int inputSize, int trials)
        {
            double total = 0.0;
            var randNums = new IComparable[inputSize];

            var rand = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < trials; i++)
            {
                for (int j = 0; j < inputSize; j++)
                    randNums[j] = rand.NextDouble();

                total += Time(alg, randNums);
            }

            return total;
        }

        public static double Time(string alg, IComparable[] collection)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            switch (alg)
            {
                case "Insertion":
                    Insertion.Sort(collection);
                    break;
                case "Selection":
                    Selection.Sort(collection);
                    break;
                case "Shell":
                    Shell.Sort(collection);
                    break;
                case "MergeRecursive":
                    MergeRecursive.Sort(collection);
                    break;
                case "MergeIterative":
                    MergeIterative.Sort(collection);
                    break;
                case "Quick":
                    Quick.Sort(collection);
                    break;
                case "QuickOptimized":
                    QuickOptimized.Sort(collection);
                    break;
                case "Heap":
                    Heap.Sort(collection);
                    break;
                case "Library":
                    Array.Sort(collection);
                    break;
            }

            stopwatch.Stop();

            Debug.Assert(SortOperations.IsSorted(collection));

            return stopwatch.ElapsedMilliseconds;
        }
    }
}
