﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Sorts
{
    public static class Heap
    {
        // Indicies are "off-by-one" to support 1-based indexing.
        private static void Exchange(object[] collection, int i, int j)
        {
            object temp = collection[i - 1];
            collection[i - 1] = collection[j - 1];
            collection[j - 1] = temp;
        }

        // Indicies are "off-by-one" to support 1-based indexing.
        private static bool IsLess(IComparable[] collection, int i, int j)
        {
            return collection[i - 1].CompareTo(collection[j - 1]) < 0;
        }

        private static void Sink(IComparable[] collection, int k, int n)
        {
            while (2 * k <= n)
            {
                int j = 2 * k;
                if (j < n && IsLess(collection, j, j+1))
                    j++;

                if (!IsLess(collection, k, j))
                    break;

                Exchange(collection, k, j);
                k = j;
            }
        }

        public static void Sort(IComparable[] collection)
        {
            int length = collection.Length;
            for (int i = length / 2; i >= 1; i--)
                Sink(collection, i, length);

            while (length > 1)
            {
                Exchange(collection, 1, length--);
                Sink(collection, 1, length);
            }
        }
    }
}
