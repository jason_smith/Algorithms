﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Sorts
{
    public static class Selection
    {
        public static void Sort(IComparable[] collection)
        {
            for (int i = 0; i < collection.Length; i++)
            {
                int min = i;
                for (int j = i + 1; j < collection.Length; j++)
                {
                    if (SortOperations.LessThan(collection[j], collection[min]))
                        min = j;
                }

                SortOperations.Exchange(collection, i, min);
            }
        }
    }
}
