﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Sorts
{
    public static class Insertion
    {
        public static void Sort(IComparable[] collection)
        {
            Sort(collection, 1, collection.Length - 1);
        }

        public static void Sort(IComparable[] collection, int lo, int hi)
        {
            for (int i = lo; i <= hi; i++)
            {
                for (int j = i; j > lo && SortOperations.LessThan(collection[j], collection[j - 1]); j--)
                    SortOperations.Exchange(collection, j, j - 1);
            }
        }
    }
}
