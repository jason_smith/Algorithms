﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Algorithms.Graphs;
using Algorithms.Sorts;
using Algorithms.Strings;

namespace Algorithms
{
    class Program
    {
        //// compression
        //static void Main(string[] args)
        //{
        //    string src = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "currentTasks.txt");
        //    string dest = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "currentTasks_COMP");

        //    Console.WriteLine("Compressing...");
        //    //RunLengthEncoding.Compress(src, dest);
        //    HuffmanCompression.Compress(src, dest);
        //    Console.WriteLine("Finished. Press enter to expand.");
        //    Console.ReadKey();

        //    Console.WriteLine("Expanding...");
        //    string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "currentTasks_EXP.txt");
        //    HuffmanCompression.Expand(dest, path);
        //    //RunLengthEncoding.Expand(dest, path);

        //    Console.WriteLine("Finished.");
        //    Console.ReadKey();
        //}

        //// tries
        //static void Main(string[] args)
        //{
        //    string txt = File.ReadAllText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "wordlist.txt"));
        //    string[] words = txt.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);

        //    Dictionary<string, string> dictionary = LoadDictionary(words);
        //    Trie<string> trie = LoadTrie(words);
        //    TernarySearchTrie<string> tst = LoadTST(words);

        //    var lookupWords = new string[1000];
        //    for (int i = 0; i < 1000; i++)
        //    {
        //        int randIndex = StdRandom.Uniform(words.Length);
        //        lookupWords[i] = words[randIndex];
        //    }

        //    LookUp(dictionary, lookupWords);
        //    LookUp(trie, lookupWords);
        //    LookUp(tst, lookupWords);

        //    //var trie = new Trie<string>(Alphabet.Lowercase);
        //    //var trie = new TernarySearchTrie<string>();
        //    //trie.Add("she", "one");
        //    //trie.Add("sells", "two");
        //    //trie.Add("sea", "three");
        //    //trie.Add("shells", "four");
        //    //trie.Add("by", "five");
        //    //trie.Add("the", "six");
        //    //trie.Add("sea", "seven");
        //    //trie.Add("shore", "eight");

        //    //Console.WriteLine("num keys = " + trie.Count);

        //    //IEnumerable<string> keys = trie.GetKeys();
        //    //foreach (string key in keys)
        //    //{
        //    //    Console.WriteLine(key);
        //    //    trie.Remove(key);
        //    //}

        //    //Console.WriteLine("num keys = " + trie.Count);

        //    Console.ReadKey();
        //}


        private static long AverageTime(long[] times)
        {
            long total = 0;
            for (int i = 0; i < times.Length; i++)
                total += times[i];

            return total / times.Length;
        }


        private static void LookUp(Dictionary<string, string> dict, string[] words)
        {
            var times = new long[20];

            for (int i = 0; i < times.Length; i++)
            {
                var stopwatch = new Stopwatch();
                stopwatch.Start();

                foreach (string word in words)
                {
                    string w = dict[word];
                }

                stopwatch.Stop();
                times[i] = stopwatch.ElapsedMilliseconds;
            }



            Console.WriteLine("Lookup Dictionary: " + AverageTime(times));
        }


        private static void LookUp(Trie<string> trie, string[] words)
        {
            var times = new long[20];

            for (int i = 0; i < times.Length; i++)
            {
                var stopwatch = new Stopwatch();
                stopwatch.Start();

                foreach (string word in words)
                {
                    string w = trie.GetValue(word);
                }

                stopwatch.Stop();
                times[i] = stopwatch.ElapsedMilliseconds;
            }

            Console.WriteLine("Lookup Trie: " + AverageTime(times));
        }


        private static void LookUp(TernarySearchTrie<string> tst, string[] words)
        {
            var times = new long[20];

            for (int i = 0; i < times.Length; i++)
            {
                var stopwatch = new Stopwatch();
                stopwatch.Start();

                foreach (string word in words)
                {
                    string w = tst.GetValue(word);
                }

                stopwatch.Stop();
                times[i] = stopwatch.ElapsedMilliseconds;
            }

            Console.WriteLine("Lookup TST: " + AverageTime(times));
        }


        private static Dictionary<string, string> LoadDictionary(string[] words)
        {
            Dictionary<string, string> dictionary = null;
            var times = new long[20];

            for (int i = 0; i < times.Length; i++)
            {
                dictionary = new Dictionary<string, string>();

                var stopwatch = new Stopwatch();
                stopwatch.Start();

                foreach (string word in words)
                    dictionary.Add(word, word);

                stopwatch.Stop();
                times[i] = stopwatch.ElapsedMilliseconds;
            }

            Console.WriteLine("Load Dictionary: " + AverageTime(times));

            return dictionary;
        }


        private static Trie<string> LoadTrie(string[] words)
        {
            Trie<string> trie = null;
            var times = new long[20];

            for (int i = 0; i < times.Length; i++)
            {
                trie = new Trie<string>(Alphabet.DnaLower);

                var stopwatch = new Stopwatch();
                stopwatch.Start();

                foreach (string word in words)
                    trie.Add(word, word);

                stopwatch.Stop();
                times[i] = stopwatch.ElapsedMilliseconds;
            }

            Console.WriteLine("Load Trie: " + AverageTime(times));

            return trie;
        }


        private static TernarySearchTrie<string> LoadTST(string[] words)
        {
            TernarySearchTrie<string> tst = null;
            var times = new long[20];

            for (int i = 0; i < times.Length; i++)
            {
                tst = new TernarySearchTrie<string>();

                var stopwatch = new Stopwatch();
                stopwatch.Start();

                foreach (string word in words)
                    tst.Add(word, word);

                stopwatch.Stop();
                times[i] = stopwatch.ElapsedMilliseconds;
            }

            Console.WriteLine("Load TST: " + AverageTime(times));

            return tst;
        }

        private static EdgeWeightedDigraph BuildEWD(string filePath)
        {
            string text = File.ReadAllText(filePath);
            using (var reader = new StringReader(text))
            {
                string line = reader.ReadLine();
                int numVertices = int.Parse(line);

                var graph = new EdgeWeightedDigraph(numVertices);

                line = reader.ReadLine();
                int numEdges = int.Parse(line);

                for (int i = 0; i < numEdges; i++)
                {
                    line = reader.ReadLine();
                    string[] lineParts = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                    int vertex1 = int.Parse(lineParts[0]);
                    int vertex2 = int.Parse(lineParts[1]);
                    double weight = double.Parse(lineParts[2]);

                    var edge = new DirectedEdge(vertex1, vertex2, weight);
                    graph.AddEdge(edge);
                }

                return graph;
            }
        }

        //// queues
        //static void Main(string[] args)
        //{
        //    // insert a bunch of strings
        //    string[] strings = { "it", "was", "the", "best", "of", "times", "it", "was", "the", "worst" };

        //    var pq = new IndexMinPriorityQueue<String>(strings.Length);
        //    for (int i = 0; i < strings.Length; i++)
        //    {
        //        pq.Enqueue(i, strings[i]);
        //    }

        //    // delete and print each key
        //    while (pq.Count > 0)
        //    {
        //        int i = pq.Dequeue();
        //        Console.WriteLine($"{i} {strings[i]}");
        //    }

        //    Console.WriteLine();

        //    // reinsert the same strings
        //    for (int i = 0; i < strings.Length; i++)
        //    {
        //        pq.Enqueue(i, strings[i]);
        //    }

        //    // print each key using the iterator
        //    foreach (int i in pq)
        //    {
        //        Console.WriteLine($"{i} {strings[i]}");
        //    }

        //    while (pq.Count > 0)
        //    {
        //        pq.Dequeue();
        //    }

        //    Console.ReadKey();
        //}

        // digraphs
        static void Main(string[] args)
        {
            EdgeWeightedDigraph graph = BuildEWD(@"C:\Users\jsmith4\Desktop\1000EWD.txt");

            var dij = new DijkstraShortestPath(graph, 0);
            if (dij.HasPathTo(990))
            {
                IEnumerable<DirectedEdge> edges = dij.GetPathTo(990);
                foreach (DirectedEdge edge in edges)
                {
                    Console.WriteLine(edge.ToString());
                }
            }
            else
                Console.WriteLine("No path exists.");

            Console.Write(graph.ToString());
            Console.ReadKey();
        }

        //// undirected graphs
        //static void Main(string[] args)
        //{
        //    string filePath = @"C:\Users\jsmith\Desktop\Algorithms\12 Undirected Graphs\movies.txt";
        //    Task<SymbolGraph> task = GraphFactory.BuildSymbolGraphAsync(filePath, '/', GraphType.Undirected);
        //    task.Wait();

        //    SymbolGraph graph = task.Result;

        //    Console.Write(graph.ToString());
        //    Console.ReadKey();
        //}

        // sorting
        //static void Main(string[] args)
        //{
        //    string alg1 = args[0];
        //    string alg2 = args[1];
        //    int inputSize = int.Parse(args[2]);
        //    int trials = int.Parse(args[3]);

        //    double time1 = SortCompare.TimeRandomInput(alg1, inputSize, trials);
        //    double time2 = SortCompare.TimeRandomInput(alg2, inputSize, trials);

        //    double ratio;
        //    string fasterAlg;
        //    if (time1 > time2)
        //    {
        //        ratio = time1 / time2;
        //        fasterAlg = alg2;
        //    }
        //    else
        //    {
        //        ratio = time2 / time1;
        //        fasterAlg = alg1;
        //    }

        //    Console.WriteLine("Resulting times are averages of {0} trials.", trials);
        //    Console.WriteLine("{0} sorted {1} random numbers in {2} milliseconds.", alg1, inputSize, time1);
        //    Console.WriteLine("{0} sorted {1} random numbers in {2} milliseconds.", alg2, inputSize, time2);
        //    Console.WriteLine(" {0} is {1:0.0} times faster.", fasterAlg, ratio);

        //    Console.ReadKey();
        //}
    }
}
