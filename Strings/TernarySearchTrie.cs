﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Strings
{
    public class TernarySearchTrie<T> where T : class
    {
        #region Fields

        Node<T> _root;
        int _count;

        #endregion

        #region Constructors


        public TernarySearchTrie()
        {
        }

        #endregion

        #region Properties


        public T this[string key]
        {
            get { return this.GetValue(key); }
            set { this.Add(key, value); }
        }


        public int Count
        {
            get { return _count; }
        }

        #endregion

        #region Methods


        public void Add(string key, T value)
        {
            this.ValidateKey(key);
            _root = this.Add(key, value, _root, charIndex: 0);
        }


        private Node<T> Add(string key, T value, Node<T> node, int charIndex)
        {
            char c = key[charIndex];

            if (node == null)
                node = new Node<T>() { Character = c };

            if (c < node.Character)
                node.Left = this.Add(key, value, node.Left, charIndex);
            else if (c > node.Character)
                node.Right = this.Add(key, value, node.Right, charIndex);
            else if (charIndex < key.Length - 1)
                node.Center = this.Add(key, value, node.Center, charIndex + 1);
            else
            {
                if (node.Value == null)
                    _count++;

                node.Value = value;
            }

            return node;
        }


        private Node<T> FindNode(string key, Node<T> node, int charIndex)
        {
            if (node == null)
                throw new KeyNotFoundException();

            char c = key[charIndex];
            if (c < node.Character)
                return this.FindNode(key, node.Left, charIndex);
            else if (c > node.Character)
                return this.FindNode(key, node.Right, charIndex);
            else if (charIndex < key.Length - 1)
                return this.FindNode(key, node.Center, charIndex + 1);
            else
                return node;
        }


        public IEnumerable<string> GetKeys()
        {
            var keys = new LinkedList<string>();
            this.GetKeys(_root, "", keys);
            return keys;
        }


        private void GetKeys(Node<T> node, string prefix, LinkedList<string> keys)
        {
            if (node == null)
                return;

            this.GetKeys(node.Left, prefix, keys);
            this.GetKeys(node.Center, prefix + node.Character, keys);
            this.GetKeys(node.Right, prefix, keys);

            if (node.Value != null)
                keys.AddLast(prefix + node.Character);
        }


        public IEnumerable<string> GetKeysWithPrefix(string prefix)
        {
            this.ValidateKey(prefix);

            var keys = new LinkedList<string>();

            try
            {
                Node<T> node = this.FindNode(prefix, _root, charIndex: 0);
                if (node.Value != null)
                    keys.AddLast(prefix);

                this.GetKeys(node.Center, prefix, keys);
            }
            catch (KeyNotFoundException)
            {
            }

            return keys;
        }


        public T GetValue(string key)
        {
            this.ValidateKey(key);
            T value = this.GetValue(key, _root, charIndex: 0);
            return value;
        }


        private T GetValue(string key, Node<T> node, int charIndex)
        {
            if (node == null)
                throw new KeyNotFoundException();

            char c = key[charIndex];
            if (c < node.Character)
                return this.GetValue(key, node.Left, charIndex);

            if (c > node.Character)
                return this.GetValue(key, node.Right, charIndex);

            if (charIndex < key.Length - 1)
                return this.GetValue(key, node.Center, charIndex + 1);

            return node.Value;
        }


        public void Remove(string key)
        {
            this.ValidateKey(key);
            _root = this.Remove(key, _root, charIndex: 0);
            _count--;
        }


        private Node<T> Remove(string key, Node<T> node, int charIndex)
        {
            if (node == null)
                throw new KeyNotFoundException();

            char c = key[charIndex];
            if (c < node.Character)
                node.Left = this.Remove(key, node.Left, charIndex);
            else if (c > node.Character)
                node.Right = this.Remove(key, node.Right, charIndex);
            else if (charIndex < key.Length - 1)
                node.Center = this.Remove(key, node.Center, charIndex + 1);
            else
                node.Value = null;

            if (node.Left == null && node.Right == null && node.Center == null && node.Value == null)
                return null;

            return node;
        }


        private void ValidateKey(string key)
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException("key");
        }

        #endregion

        #region Nested class: Node

        class Node<T>
        {
            public Node<T> Center;
            public Node<T> Left;
            public Node<T> Right;
            public T Value;
            public char Character;

            public Node()
            {
            }

            public Node(char c, T value)
            {
                this.Character = c;
                this.Value = value;
            }
        }

        #endregion
    }
}
