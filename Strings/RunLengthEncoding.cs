﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Strings
{
    public static class RunLengthEncoding
    {
        #region Fields

        const int MaxValue = 256;
        const int BitEncoding = 8;

        #endregion

        #region Methods


        public static void Compress(string sourceFile, string destFile)
        {
            ValidateFilePaths(sourceFile, destFile);
            
            bool oldBit = false;
            int count = 0;

            using (var binaryIn = new BinaryInput(new FileStream(sourceFile, FileMode.Open)))
            using (var binaryOut = new BinaryOutput(new FileStream(destFile, FileMode.Create)))
            {
                while (!binaryIn.IsEmpty)
                {
                    bool bit = binaryIn.ReadBit();
                    if (bit != oldBit)
                    {
                        binaryOut.Write(count, BitEncoding);
                        count = 0;
                        oldBit = !oldBit;
                    }
                    else
                    {
                        if (count == MaxValue - 1)
                        {
                            binaryOut.Write(count, BitEncoding);
                            count = 0;
                            binaryOut.Write(count, BitEncoding);
                        }
                    }

                    count++;
                }

                binaryOut.Write((char)count);
            }
        }


        public static void Expand(string sourceFile, string destFile)
        {
            ValidateFilePaths(sourceFile, destFile);

            bool bit = false;

            using (var binaryIn = new BinaryInput(new FileStream(sourceFile, FileMode.Open)))
            using (var binaryOut = new BinaryOutput(new FileStream(destFile, FileMode.Create)))
            {
                while (!binaryIn.IsEmpty)
                {
                    int run = binaryIn.ReadInt(BitEncoding);
                    for (int i = 0; i < run; i++)
                        binaryOut.Write(bit);

                    bit = !bit;
                }
            }
        }


        private static void ValidateFilePaths(string source, string dest)
        {
            if (string.IsNullOrEmpty(source))
                throw new ArgumentNullException("source");

            if (string.IsNullOrEmpty(dest))
                throw new ArgumentNullException("dest");

            if (!File.Exists(source))
                throw new FileNotFoundException();
        }

        #endregion
    }
}
