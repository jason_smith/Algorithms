﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Strings
{
    public class BinaryInput : IDisposable
    {
        #region Fields

        readonly static int EndOfFile = -1;// int.MinValue;
        readonly Stream _stream;
        int _numBitsInBuffer;
        int _buffer;

        #endregion

        #region Constructors


        public BinaryInput(Stream stream)
        {
            _stream = stream;
            this.FillBuffer();
        }

        #endregion

        #region Properties


        public bool IsEmpty
        {
            get { return _buffer == EndOfFile; }
        }

        #endregion

        #region Methods


        public void Dispose()
        {
            if (_stream != null)
                _stream.Dispose();
        }


        private void FillBuffer()
        {
            try
            {
                _buffer = _stream.ReadByte();
                _numBitsInBuffer = 8;
            }
            catch (IOException)
            {
                _buffer = EndOfFile;
                _numBitsInBuffer = -1;
            }
        }


        public bool ReadBit()
        {
            this.ValidateRead();

            _numBitsInBuffer--;
            bool bit = ((_buffer >> _numBitsInBuffer) & 1) == 1;

            if (_numBitsInBuffer == 0)
                this.FillBuffer();

            return bit;
        }


        public byte ReadByte()
        {
            char c = this.ReadChar();
            return (byte)(c & 0xff);
        }


        public char ReadChar()
        {
            this.ValidateRead();

            int tempBuf = _buffer;

            // special case when aligned byte
            if (_numBitsInBuffer == 8)
                this.FillBuffer();
            else
            {
                // combine last n bits of current buffer with first 8-n bits of the new buffer
                tempBuf <<= (8 - _numBitsInBuffer);
                int tempNumBits = _numBitsInBuffer;
                this.FillBuffer();

                this.ValidateRead();

                _numBitsInBuffer = tempNumBits;
                tempBuf |= (int)((uint)_buffer >> _numBitsInBuffer);
            }

            return (char)(tempBuf & 0xff);
        }


        //public char ReadChar(int numBits)
        //{
        //    if (numBits < 1 || numBits > 16)
        //        throw new ArgumentException("'numBits' must be between 1 and 16.");

        //    if (numBits == 8)
        //        return this.ReadChar();

        //    char c;
        //    for (int i = 0; i < numBits; i++)
        //    {
        //        c <<= 1;
        //        bool bit = this.ReadBit();
        //        if (bit)
        //            c |= 1;
        //    }

        //    return c;
        //}


        public double ReadDouble()
        {
            long x = this.ReadLong();
            return Convert.ToDouble(x);
        }


        public float ReadFloat()
        {
            int x = this.ReadInt();
            return Convert.ToSingle(x);
        }


        public int ReadInt()
        {
            this.ValidateRead();

            int x = 0;
            for (int i = 0; i < 4; i++)
            {
                char c = this.ReadChar();
                x <<= 8;
                x |= c;
            }

            return x;
        }


        public int ReadInt(int numBits)
        {
            if (numBits < 1 || numBits > 32)
                throw new ArgumentException("'numBits' must be between 1 and 32.");

            if (numBits == 32)
                return this.ReadInt();

            int x = 0;
            for (int i = 0; i < numBits; i++)
            {
                x <<= 1;
                bool bit = this.ReadBit();
                if (bit)
                    x |= 1;
            }

            return x;
        }


        public long ReadLong()
        {
            this.ValidateRead();

            long x = 0;
            for (int i = 0; i < 8; i++)
            {
                char c = this.ReadChar();
                x <<= 8;
                x |= c;
            }

            return x;
        }


        //public short ReadShort()
        //{
        //    this.ValidateRead();

        //    short x = 0;
        //    for (int i = 0; i < 2; i++)
        //    {
        //        char c = this.ReadChar();
        //        x <<= 8;
        //        x |= c;
        //    }

        //    return x;
        //}


        public string ReadString()
        {
            this.ValidateRead();

            var sb = new StringBuilder();
            while (!this.IsEmpty)
            {
                char c = this.ReadChar();
                sb.Append(c);
            }

            return sb.ToString();
        }


        private void ValidateRead()
        {
            if (this.IsEmpty)
                throw new InvalidOperationException("Input stream is empty.");
        }

        #endregion
    }
}
