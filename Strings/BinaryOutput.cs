﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Strings
{
    public class BinaryOutput : IDisposable
    {
        #region Fields

        readonly Stream _stream;
        int _numBitsInBuffer;
        int _buffer;

        #endregion

        #region Constructors


        public BinaryOutput(Stream stream)
        {
            _stream = stream;
        }

        #endregion

        #region Properties




        #endregion

        #region Methods


        private void ClearBuffer()
        {
            if (_numBitsInBuffer == 0)
                return;

            if (_numBitsInBuffer > 0)
                _buffer <<= (8 - _numBitsInBuffer);

            byte buf = Convert.ToByte(_buffer);
            _stream.WriteByte(buf);

            _numBitsInBuffer = 0;
            _buffer = 0;
        }


        public void Dispose()
        {
            if (_stream != null)
            {
                _stream.Flush();
                _stream.Dispose();
            }
        }


        public void Flush()
        {
            this.ClearBuffer();
            _stream.Flush();
        }


        private int UnsignedShiftRight(int x, int shiftLength)
        {
            return (int)((uint)x >> shiftLength);
        }


        private long UnsignedShiftRight(long x, int shiftLength)
        {
            return (long)((ulong)x >> shiftLength);
        }


        public void Write(bool x)
        {
            this.WriteBit(x);
        }


        public void Write(byte x)
        {
            this.WriteByte(x & 0xff);
        }


        public void Write(int x)
        {
            this.WriteByte(this.UnsignedShiftRight(x, 24) & 0xff);
            this.WriteByte(this.UnsignedShiftRight(x, 16) & 0xff);
            this.WriteByte(this.UnsignedShiftRight(x, 8) & 0xff);
            this.WriteByte(this.UnsignedShiftRight(x, 0) & 0xff);
        }


        public void Write(int x, int numBits)
        {
            if (numBits == 32)
                this.Write(x);
            else if (numBits < 1 || numBits > 32)
                throw new ArgumentException("'numBits' must be between 1 and 32. numBits = " + numBits.ToString());
            else if (x < 0 || x >= (1 << numBits))
                throw new ArgumentException("Invalid " + numBits.ToString() + "-bit char = " + x.ToString());
            else
            {
                for (int i = 0; i < numBits; i++)
                {
                    bool bit = (this.UnsignedShiftRight(x, numBits - i - 1) & 1) == 1;
                    this.WriteBit(bit);
                }
            }
        }


        public void Write(double x)
        {
            this.Write(BitConverter.DoubleToInt64Bits(x));
        }


        public void Write(short x)
        {
            this.WriteByte(this.UnsignedShiftRight(x, 8) & 0xff);
            this.WriteByte(this.UnsignedShiftRight(x, 0) & 0xff);
        }


        public void Write(long x)
        {
            this.WriteByte((int)(this.UnsignedShiftRight(x, 56) & 0xff));
            this.WriteByte((int)(this.UnsignedShiftRight(x, 48) & 0xff));
            this.WriteByte((int)(this.UnsignedShiftRight(x, 40) & 0xff));
            this.WriteByte((int)(this.UnsignedShiftRight(x, 32) & 0xff));
            this.WriteByte((int)(this.UnsignedShiftRight(x, 24) & 0xff));
            this.WriteByte((int)(this.UnsignedShiftRight(x, 16) & 0xff));
            this.WriteByte((int)(this.UnsignedShiftRight(x, 8) & 0xff));
            this.WriteByte((int)(this.UnsignedShiftRight(x, 0) & 0xff));
        }


        public void Write(char x)
        {
            if (x < 0 || x > 255)
                throw new ArgumentException("Invalid 8-bit character = " + x.ToString());

            this.WriteByte(x);
        }


        public void Write(char x, int numBits)
        {
            if (numBits == 8)
                this.Write(x);
            else if (numBits < 1 || numBits > 16)
                throw new ArgumentException("'numBits' must be between  1 and 16. numBits = " + numBits.ToString());
            else if (x >= (1 << numBits))
                throw new ArgumentException("Invalid " + numBits.ToString() + "-bit character = " + x.ToString());
            else
            {
                for (int i = 0; i < numBits; i++)
                {
                    bool bit = (this.UnsignedShiftRight(x, numBits - i - 1) & 1) == 1;
                    this.WriteBit(bit);
                }
            }
        }


        public void Write(string str)
        {
            if (str == null)
                throw new ArgumentNullException("str");

            for (int i = 0; i < str.Length; i++)
                this.Write(str[i]);
        }


        public void Write(string str, int numBits)
        {
            if (str == null)
                throw new ArgumentNullException("str");

            for (int i = 0; i < str.Length; i++)
                this.Write(str[i], numBits);
        }


        private void WriteBit(bool bit)
        {
            // add bit to buffer
            _buffer <<= 1;
            if (bit)
                _buffer |= 1;

            // if buffer is full (8 bits), write out as single byte
            _numBitsInBuffer++;
            if (_numBitsInBuffer == 8)
                this.ClearBuffer();
        }


        private void WriteByte(int x)
        {
            if (x < 0 || x > 255)
                throw new ArgumentException("'x' must be between 0 and 255.");

            // optimized if byte-aligned
            if (_numBitsInBuffer == 0)
            {
                byte xToByte = Convert.ToByte(x);
                _stream.WriteByte(xToByte);
            }
            else
            {
                // otherwise write one bit at a time
                for (int i = 0; i < 8; i++)
                {
                    int shift = this.UnsignedShiftRight(x, 8 - i - 1);
                    bool bit = (shift & 1) == 1;
                    this.WriteBit(bit);
                }
            }
        }

        #endregion
    }
}
