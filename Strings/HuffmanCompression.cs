﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Algorithms.Sorts;

namespace Algorithms.Strings
{
    public class HuffmanCompression
    {
        #region Fields

        const int Size = 256; // alphabet size of extended ASCII

        #endregion

        #region Methods


        //private static void BuildCode(string[] table, Node node, string str)
        //{
        //    if (!node.IsLeaf)
        //    {
        //        BuildCode(table, node.Left, str + '0');
        //        BuildCode(table, node.Right, str + '1');
        //    }
        //    else
        //        table[node.Character] = str;
        //}


        private static void BuildCodeTable(Dictionary<char, string> table, Node node, string code)
        {
            if (!node.IsLeaf)
            {
                BuildCodeTable(table, node.Left, code + '0');
                BuildCodeTable(table, node.Right, code + '1');
            }
            else
                table.Add(node.Character, code);
        }


        private static Node BuildTrie(int[] frequencies)
        {
            // initialize queue with singleton trees
            var queue = new MinPriorityQueue<Node>();
            for (int i = 0; i < Size; i++)
            {
                if (frequencies[i] > 0)
                {
                    var node = new Node((char)i, frequencies[i], null, null);
                    queue.Enqueue(node);
                }
            }

            // special case where there is only one character with a nonzero frequency
            if (queue.Count == 1)
            {
                char c = (frequencies['\0'] == 0) ? '\0' : (char)1;
                queue.Enqueue(new Node(c, 0, null, null));
            }

            while (queue.Count > 1)
            {
                Node left = queue.Dequeue();
                Node right = queue.Dequeue();
                Node parent = new Node('\0', left.Frequency + right.Frequency, left, right);
                queue.Enqueue(parent);
            }

            return queue.Dequeue();
        }


        public static Node Compress(string sourceFile, string destFile)
        {
            ValidateFilePaths(sourceFile, destFile);

            string text = File.ReadAllText(sourceFile);
            char[] input = text.ToCharArray();
            int[] freqs = GetFrequencyCounts(input);

            Node root = BuildTrie(freqs);

            var codeTable = new Dictionary<char, string>();
            BuildCodeTable(codeTable, root, "");

            using (var binaryOut = new BinaryOutput(new FileStream(destFile, FileMode.Create)))
            {
                // write trie for decoder
                WriteTrie(root, binaryOut);

                // write number of bytes in original uncompressed message
                binaryOut.Write(input.Length);

                // use Huffman code to encode input
                for (int i = 0; i < input.Length; i++)
                {
                    string code = codeTable[input[i]];

                    for (int j = 0; j < code.Length; j++)
                    {
                        if (code[j] == '0')
                            binaryOut.Write(false);
                        else if (code[j] == '1')
                            binaryOut.Write(true);
                        else
                            throw new InvalidDataException("Invalid state detected.");
                    }
                }
            }

            return root;
        }


        public static Node Expand(string sourceFile, string destFile)
        {
            ValidateFilePaths(sourceFile, destFile);

            using (var binaryIn = new BinaryInput(new FileStream(sourceFile, FileMode.Open)))
            using (var binaryOut = new BinaryOutput(new FileStream(destFile, FileMode.Create)))
            {
                Node root = ReadTrie(binaryIn);
                return root;

                // read in number of bytes to write
                int length = binaryIn.ReadInt();

                // decode using Huffman trie
                for (int i = 0; i < length; i++)
                {
                    Node node = root;

                    while (!node.IsLeaf)
                    {
                        bool bit = binaryIn.ReadBit();
                        if (bit)
                            node = node.Right;
                        else
                            node = node.Left;
                    }

                    binaryOut.Write(node.Character, 8);
                }
            }
        }


        private static int[] GetFrequencyCounts(char[] input)
        {
            var freqs = new int[Size];
            for (int i = 0; i < input.Length; i++)
                freqs[input[i]]++;

            return freqs;
        }


        private static Node ReadTrie(BinaryInput binaryIn)
        {
            bool isLeaf = binaryIn.ReadBit();
            if (isLeaf)
            {
                char c = binaryIn.ReadChar();
                return new Node(c, -1, null, null);
            }
            else
                return new Node('\0', -1, ReadTrie(binaryIn), ReadTrie(binaryIn));
        }


        private static void ValidateFilePaths(string source, string dest)
        {
            if (string.IsNullOrEmpty(source))
                throw new ArgumentNullException("source");

            if (string.IsNullOrEmpty(dest))
                throw new ArgumentNullException("dest");

            if (!File.Exists(source))
                throw new FileNotFoundException();
        }


        private static void WriteTrie(Node node, BinaryOutput binaryOut)
        {
            if (node.IsLeaf)
            {
                binaryOut.Write(true);
                binaryOut.Write(node.Character, 8);
            }
            else
            {
                binaryOut.Write(false);
                WriteTrie(node.Left, binaryOut);
                WriteTrie(node.Right, binaryOut);
            }
        }

        #endregion

        #region Nested Class: Node

        public class Node : IComparable<Node>
        {
            readonly List<Node> _children;
            readonly Node _left;
            readonly Node _right;
            readonly char _char;
            readonly int _freq;

            public Node(char c, int freq, Node left, Node right)
            {
                _char = c;
                _freq = freq;
                _left = left;
                _right = right;

                _children = new List<Node>(2);
                if (_left != null)
                    _children.Add(_left);
                if (_right != null)
                    _children.Add(_right);
            }

            public char Character
            {
                get { return _char; }
            }

            public int CharVal
            {
                get { return (int)_char; }
            }

            public IEnumerable<Node> Children
            {
                get { return _children; }
            }

            public int Frequency
            {
                get { return _freq; }
            }

            public bool IsLeaf
            {
                get { return (_left == null) && (_right == null); }
            }

            public Node Left
            {
                get { return _left; }
            }

            public string PrintableChar
            {
                get
                {
                    if (_char < 33)
                    {
                        if (_char == 0)
                            return "\\0";
                        if (_char == 1)
                            return "\\1";
                        if (_char == 9)
                            return "\\t";
                        if (_char == 10)
                            return "\\n";
                        if (_char == 13)
                            return "\\r";
                        if (_char == 32)
                            return "Space";

                        return "Unknown char";
                    }
                    else
                        return _char.ToString();
                }
            }

            public Node Right
            {
                get { return _right; }
            }

            public int CompareTo(Node other)
            {
                return _freq - other._freq;
            }
        }

        #endregion
    }
}
