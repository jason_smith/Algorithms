﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Strings
{
    public class Alphabet
    {
        #region Fields

        public readonly static Alphabet Binary = new Alphabet("01");
        public readonly static Alphabet Octal = new Alphabet("01234567");
        public readonly static Alphabet Decimal = new Alphabet("0123456789");
        public readonly static Alphabet Hexadecimal = new Alphabet("0123456789ABCDEF");
        public readonly static Alphabet DnaUpper = new Alphabet("ACTG");
        public readonly static Alphabet DnaLower = new Alphabet("actg");
        public readonly static Alphabet Lowercase = new Alphabet("abcdefghijklmnopqrstuvwxyz");
        public readonly static Alphabet Uppercase = new Alphabet("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        public readonly static Alphabet Base64 = new Alphabet("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/");
        public readonly static Alphabet Ascii = new Alphabet(128);
        public readonly static Alphabet ExtendedAscii = new Alphabet(256);
        public readonly static Alphabet Unicode16 = new Alphabet(65536);

        readonly char[] _alphabet;  // characters in the alphabet
        readonly int[] _inverse;    // indicies
        readonly int _radix;        // radix of the alphabet

        #endregion

        #region Constructors


        public Alphabet() : this(256)
        {
        }


        public Alphabet(string alpha)
        {
            this.ValidateAlphabet(alpha);

            _alphabet = alpha.ToCharArray();
            _radix = _alphabet.Length;
            _inverse = new int[char.MaxValue];

            for (int i = 0; i < _inverse.Length; i++)
                _inverse[i] = -1;

            for (int i = 0; i < _radix; i++)
                _inverse[_alphabet[i]] = i;
        }


        private Alphabet(int numCharacters)
        {
            _radix = numCharacters;
            _alphabet = new char[numCharacters];
            _inverse = new int[numCharacters];

            for (int i = 0; i < _radix; i++)
                _alphabet[i] = (char)i;

            for (int i = 0; i < _radix; i++)
                _inverse[i] = i;
        }

        #endregion

        #region Properties


        public int Radix
        {
            get { return _radix; }
        }

        #endregion

        #region Methods


        public bool Contains(char c)
        {
            bool isContained = (_inverse[c] != -1);
            return isContained;
        }


        public char ToChar(int index)
        {
            if (index < 0 || index >= _radix)
                throw new IndexOutOfRangeException();

            return _alphabet[index];
        }


        public char[] ToChars(int[] indices)
        {
            var chars = new char[indices.Length];
            for (int i = 0; i < indices.Length; i++)
                chars[i] = this.ToChar(indices[i]);

            return chars;
        }


        public int ToIndex(char c)
        {
            if (c >= _inverse.Length || _inverse[c] == -1)
                throw new ArgumentException("Character '" + c + "' not in alphabet.");

            return _inverse[c];
        }


        public int[] ToIndices(string str)
        {
            char[] source = str.ToCharArray();
            var indices = new int[str.Length];

            for (int i = 0; i < source.Length; i++)
                indices[i] = this.ToIndex(source[i]);

            return indices;
        }


        public string ToString(int[] indices)
        {
            char[] chars = this.ToChars(indices);
            var str = new String(chars);
            return str;
        }


        private void ValidateAlphabet(string alpha)
        {
            // ensure alphabet contains no duplicate characters
            var inspected = new bool[char.MaxValue];
            for (int i = 0; i < alpha.Length; i++)
            {
                char c = alpha[i];
                if (inspected[c])
                    throw new ArgumentException("Invalid alphabet: repeated character = '" + c + "'.");

                inspected[c] = true;
            }
        }

        #endregion
    }
}
