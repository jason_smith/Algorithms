﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Strings
{
    public class Trie<T> where T : class
    {
        #region Fields

        readonly Alphabet _alphabet;
        readonly Node<T> _root;
        int _count;

        #endregion

        #region Constructors


        public Trie(Alphabet alphabet)
        {
            _alphabet = alphabet;
            _root = new Node<T>(alphabet.Radix);
        }

        #endregion

        #region Properties


        public T this[string key]
        {
            get { return this.GetValue(key); }
            set { this.Add(key, value); }
        }


        public int Count
        {
            get { return _count; }
        }

        #endregion

        #region Methods


        public void Add(string key, T value)
        {
            this.ValidateKey(key);
            this.Add(key, value, _root, charIndex: 0);
        }


        private void Add(string key, T value, Node<T> node, int charIndex)
        {
            int alphaIndex = _alphabet.ToIndex(key[charIndex]);
            Node<T> child = node.Children[alphaIndex];
            if (child == null)
            {
                child = new Node<T>(_alphabet.Radix);
                node.Children[alphaIndex] = child;
            }

            if (charIndex == key.Length - 1)
            {
                if (child.Value == null)
                    _count++;

                child.Value = value;
            }
            else
                this.Add(key, value, child, charIndex + 1);
        }


        private bool CanRemove(Node<T> node)
        {
            for (int i = 0; i < node.Children.Length; i++)
            {
                Node<T> child = node.Children[i];
                if (child != null)
                {
                    if (child.Value != null || child.HasChildren())
                        return false;
                }
            }

            return true;
        }


        private Node<T> FindNode(string key, Node<T> node, int charIndex)
        {
            int alphaIndex = _alphabet.ToIndex(key[charIndex]);
            Node<T> child = node.Children[alphaIndex];
            if (child == null)
                throw new KeyNotFoundException();

            if (charIndex == key.Length - 1)
                return child;

            return this.FindNode(key, child, charIndex + 1);
        }


        public IEnumerable<string> GetKeys()
        {
            var keys = new LinkedList<string>();
            this.GetKeys(_root, "", keys);
            return keys;
        }


        private void GetKeys(Node<T> node, string prefix, LinkedList<string> keys)
        {
            for (int i = 0; i < node.Children.Length; i++)
            {
                Node<T> child = node.Children[i];
                if (child != null)
                {
                    char c = _alphabet.ToChar(i);
                    if (child.Value != null)
                        keys.AddLast(prefix + c);
                    
                    this.GetKeys(child, prefix + c, keys);
                }
            }
        }


        public IEnumerable<string> GetKeysWithPrefix(string prefix)
        {
            this.ValidateKey(prefix);

            var keys = new LinkedList<string>();

            try
            {
                Node<T> node = this.FindNode(prefix, _root, charIndex: 0);
                if (node.Value != null)
                    keys.AddLast(prefix);

                this.GetKeys(node, prefix, keys);
            }
            catch (KeyNotFoundException)
            {
            }

            return keys;
        }


        public T GetValue(string key)
        {
            this.ValidateKey(key);
            Node<T> node = this.FindNode(key, _root, charIndex: 0);
            return node.Value;
        }


        public void Remove(string key)
        {
            this.ValidateKey(key);
            this.Remove(key, _root, charIndex: 0);
            _count--;
        }

        
        private bool Remove(string key, Node<T> node, int charIndex)
        {
            int alphaIndex = _alphabet.ToIndex(key[charIndex]);
            Node<T> child = node.Children[alphaIndex];
            if (child == null)
                throw new KeyNotFoundException();

            bool checkRemovable = true;
            if (charIndex == key.Length - 1)
                child.Value = null;
            else
                checkRemovable = this.Remove(key, child, charIndex + 1);

            if (checkRemovable && this.CanRemove(child))
            {
                node.Children[alphaIndex] = null;
                return true;
            }

            return false;
        }


        private void ValidateKey(string key)
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException("key");
        }

        #endregion

        #region Nested Class: Node

        class Node<T> where T : class
        {
            public readonly Node<T>[] Children;
            public T Value;

            public Node(int alphabetSize)
            {
                Children = new Node<T>[alphabetSize];
            }

            public bool HasChildren()
            {
                for (int i = 0; i < this.Children.Length; i++)
                {
                    if (this.Children[i] != null)
                        return true;
                }

                return false;
            }
        }

        #endregion
    }
}
