﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms
{
    public static class StdRandom
    {
        readonly static Random _rand;

        /// <summary>
        /// Type constructor.
        /// </summary>
        static StdRandom()
        {
            _rand = new Random();
        }

        /// <summary>
        /// Rearranges the elements of the specified array in a uniformly random order.
        /// </summary>
        /// <param name="array">The array to shuffle.</param>
        /// <exception cref="ArgumentNullException"/>
        public static void Shuffle<T>(T[] array)
        {
            if (array == null)
                throw new ArgumentNullException("array");

            int length = array.Length;
            for (int i = 0; i < length; i++)
            {
                int randIndex = i + Uniform(length - i);

                T temp = array[i];
                array[i] = array[randIndex];
                array[randIndex] = temp;
            }
        }

        /// <summary>
        /// Rearranges the elements of the specified array in a uniformly random order.
        /// </summary>
        /// <param name="array">The array to shuffle.</param>
        /// <param name="loIndex">The left endpoint (inclusive).</param>
        /// <param name="hiIndex">The right endpoint (inclusive).</param>
        /// <exception cref="ArgumentNullException"/>
        /// <exception cref="IndexOutOfRangeException"/>
        public static void Shuffle<T>(T[] array, int loIndex, int hiIndex)
        {
            if (array == null)
                throw new ArgumentNullException("array");

            if (loIndex < 0 || loIndex > hiIndex || hiIndex >= array.Length)
                throw new IndexOutOfRangeException("Illegal subarray range.");

            for (int i = loIndex; i <= hiIndex; i++)
            {
                int randIndex = i + Uniform(hiIndex - i + 1);

                T temp = array[i];
                array[i] = array[randIndex];
                array[randIndex] = temp;
            }
        }

        /// <summary>
        /// Generates a random real number uniformly in [0, 1).
        /// </summary>
        /// <returns>A random real number.</returns>
        public static double Uniform()
        {
            return _rand.NextDouble();
        }

        /// <summary>
        /// Generates a random integer uniformly in [0, n)
        /// </summary>
        /// <param name="n">The upper limit.</param>
        /// <returns>A random integer.</returns>
        /// <exception cref="ArgumentException">Thrown when n is not a positive number.</exception>
        public static int Uniform(int n)
        {
            if (n <= 0)
                throw new ArgumentException("'n' must be positive.");

            return _rand.Next(n);
        }
    }
}
